------------------------------ MODULE IpfsMsg ------------------------------
EXTENDS Integers, Sequences, IpfsNode

(* A message is composed of an operation along with a set of data fields which
are specified as below
*)
Msgs == [
    op: {"openInit", "openAccept", "openReject"},
    from: Nodes,
    to: Nodes,
    data: Seq(STRING) ]
    
PropValidMsgFormats(msg) == 
    CASE msg.op = "openInit" -> 
        msg.data[1] \in {"valid", "invalid"} \* the first field contains credential info to validate
    [] msg.op = "openAccept" ->
        msg.data[1] \in {"valid", "invalid"} \* the first field contains credential info to validate
    [] msg.op = "openReject" ->
        Len(msg.data) = 0                    \* dont tell the attacker anything, just reject 
    [] OTHER -> FALSE

=============================================================================
\* Modification History
\* Last modified Tue Jun 16 17:26:05 CST 2015 by marco
\* Created Sun Jun 14 21:03:40 CST 2015 by marco
