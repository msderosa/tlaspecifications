--------------------------- MODULE TrustTblsCtx0 ---------------------------
EXTENDS Integers

TrustCategory == {"Probationary", "Trusted", "Fraud"}
ReservationStatus == {"Issued", "Refunded"}

Attempts == {
     << >>, 
     << "Issued" >>, << "Refunded" >>,
     << "Issued", "Issued" >>, << "Refunded", "Refunded" >>
    }
    
Customers == [
        trustCategory: TrustCategory,
        attempts: Attempts
    ]


=============================================================================
\* Modification History
\* Last modified Sun Sep 10 15:46:12 CST 2017 by ASUS
\* Created Tue Aug 29 11:57:34 CST 2017 by ASUS
