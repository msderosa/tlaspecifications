---------------------------- MODULE SignConfigs ----------------------------
EXTENDS Integers

\* well known order numbers are < 10 and aliases are > 10
Signs == {
    [street |-> 1, rlOfStreet |-> "R", wkOrderNo |-> 1, aliasOrderNo |-> 1],
    [street |-> 1, rlOfStreet |-> "L", wkOrderNo |-> 2, aliasOrderNo |-> 12],
    [street |-> 1, rlOfStreet |-> "L", wkOrderNo |-> 3, aliasOrderNo |-> 3],
    
    [street |-> 2, rlOfStreet |-> "R", wkOrderNo |-> 4, aliasOrderNo |-> 14],
    [street |-> 2, rlOfStreet |-> "R", wkOrderNo |-> 5, aliasOrderNo |-> 15],
    [street |-> 2, rlOfStreet |-> "L", wkOrderNo |-> 6, aliasOrderNo |-> 6],
    
    [street |-> 3, rlOfStreet |-> "R", wkOrderNo |-> 7, aliasOrderNo |-> 7],
    [street |-> 3, rlOfStreet |-> "L", wkOrderNo |-> 8, aliasOrderNo |-> 8],
    
    [street |-> 4, rlOfStreet |-> "R", wkOrderNo |-> 2, aliasOrderNo |-> 12],
    [street |-> 4, rlOfStreet |-> "R", wkOrderNo |-> 8, aliasOrderNo |-> 8],
    
    [street |-> 5, rlOfStreet |-> "R", wkOrderNo |-> 2, aliasOrderNo |-> 12],
    [street |-> 5, rlOfStreet |-> "R", wkOrderNo |-> 3, aliasOrderNo |-> 13],
    [street |-> 5, rlOfStreet |-> "L", wkOrderNo |-> 9, aliasOrderNo |-> 9]
    }

getWkOrderNoByRefSigns(rec, refSigns) ==
    IF rec.sign \in refSigns THEN
        rec.sign.wkOrderNo
    ELSE 
        rec.order_no
        
calcWkOrderNoByRefSigns(orderNo, refSigns) ==
    LET aliases == {s.aliasOrderNo : s \in refSigns}
    IN IF orderNo \in aliases THEN
            orderNo - 10
       ELSE
            orderNo

=============================================================================
\* Modification History
\* Last modified Wed Jun 17 18:53:06 CST 2015 by marco
\* Created Wed Jun 17 17 |->04 |->29 CST 2015 by marco
