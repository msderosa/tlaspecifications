---- MODULE MC ----
EXTENDS FollowMe, TLC

\* CONSTANT definitions @modelParameterConstants:0totalSvcArea
const_141614023143459000 == 
[sw |-> -14, ne |-> -6]
----

\* SPECIFICATION definition @modelBehaviorSpec:0
spec_141614023144460000 ==
Spec
----
\* INVARIANT definition @modelCorrectnessInvariants:0
inv_141614023145461000 ==
Inv
----
=============================================================================
\* Modification History
\* Created Sun Nov 16 20:17:11 CST 2014 by marco
