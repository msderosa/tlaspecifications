-------------------------- MODULE TestRoutingTable --------------------------
EXTENDS RoutingTable, TLC

TestMkRoutingTable == 
   LET rt1 == MkRoutingTable(255)
       rt2 == MkRoutingTable(0)
   IN /\ \A r \in 0..3: /\ rt1[r][3] = 255
                        /\ rt1[r][2] = NoNode
      /\ \A r \in 0..3: /\ rt2[r][0] = 0
                        /\ rt2[r][1] = NoNode

TestGetRTContent == 
    LET rt == [n \in {0} |-> [m \in 0..3 |-> m * m]]
    IN {0, 1, 4, 9} = GetRTableContent(rt)

TestChangeBasis ==
    /\ <<0, 1, 1, 0>> = ChangeBasis(6, 2, 4)
    /\ <<0, 1,3,2>> = ChangeBasis(30, 4, 4)

TestInsertRoutingTable ==
    LET test == MkRoutingTable(215)
        test2 == InsertRoutingTable(test, 224, 215)
        test3 == InsertRoutingTable(test2, 225, 215)
    IN /\ test[0][3] = 215
       /\ test[1][1] = 215
       /\ test2[0][3] = 215
       /\ test2[1][2] = 224
       /\ test2 = test3
       
TestLookupExact ==
    LET test == MkRoutingTable(143)
        test2 == InsertRoutingTable(test, 132, 143)
    IN /\ 132 = LookupExact(test2, 143, 133)
       /\ NoNode = LookupExact(test2, 143, 137)

=============================================================================
\* Modification History
\* Last modified Thu Jan 14 21:06:54 CST 2016 by marco
\* Created Thu Jan 07 17:28:23 CST 2016 by marco
