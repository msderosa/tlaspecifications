------------------------ MODULE TerminationDetection ------------------------
EXTENDS Integers, FiniteSets

VARIABLES counter, store, indicator

Init == /\ counter = -1
        /\ store = [counter |-> -1, tasks |-> {}]
        /\ indicator = "Passive"

TypeInv == /\ counter \in -1..10
           /\ store \in [counter: -1..10, tasks: SUBSET (0..10)]
           /\ indicator \in {"Active", "Passive"}

StoreInv == \A n \in store.tasks: n >= store.counter
IndicatorInv == IF Cardinality(store.tasks) > 0
                    THEN indicator = "Active"
                    ELSE indicator = "Passive"
                    
Inv == /\ TypeInv
       /\ StoreInv
       /\ IndicatorInv

ReleaseProcess == /\ counter < 10
                  /\ counter' = counter + 1
                  /\ store' = [store EXCEPT !.tasks = UNION {store.tasks, {counter + 1}}]
                  /\ indicator' = "Active"

ReceiveProcess == /\ Cardinality(store.tasks) > 0
                  /\ \E n \in store.tasks: LET ValidTasks == {i \in store.tasks: i >= n}
                                           IN /\ store' = [store EXCEPT !.counter = n, !.tasks = ValidTasks]
                                              /\ indicator' = IF ValidTasks = {} 
                                                                THEN "Passive"
                                                                ELSE "Active"
                  /\ UNCHANGED << counter >>
                                                                
Next == \/ ReleaseProcess
        \/ ReceiveProcess
        
Spec == Init /\ [][Next]_<< counter, store, indicator >>

=============================================================================
\* Modification History
\* Last modified Wed Oct 01 22:11:45 CST 2014 by marco
\* Created Wed Oct 01 21:25:08 CST 2014 by marco
