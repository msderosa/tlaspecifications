---------------------------- MODULE ResCreateCtx ----------------------------

TrustCategory == {"Null", "Trusted", "Fraud"}
ReservationStatus == {"", "issued", "fraud"}

(* There are five total step in the reservation creation process. The final step
queues the attempt information for fraud tracking. The queue step is always 
executed
*)
Steps == {"Validate", "Processs", "Queue"}

ResultSteps == {
    {}, {"Queue"},
    {"Validate"}, {"Validate", "Queue"},
    {"Validate", "Process"}, {"Validate", "Process", "Queue"}
}

PaymentResult == {"", "Ok", "ErrCvvAvs", "ErrOther", "Fraud"}

=============================================================================
\* Modification History
\* Last modified Tue Aug 29 12:38:42 CST 2017 by ASUS
\* Created Mon Aug 28 21:52:45 CST 2017 by ASUS
