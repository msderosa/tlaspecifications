----------------------- MODULE CalcCustomerTrustMch0 -----------------------
EXTENDS Integers, TLC, CalcCustomerTrustCtx0
VARIABLES attempt, customerHistory, pc

(* Here we model the processing of a customer attempt as done by the cron jobs *)

onAttemptForTrusted == TRUE

onAttemptForFraud == TRUE

onAttemptForProbationary == TRUE

onDisputeCallback == TRUE

onMailBounceCallback == TRUE 

(* In the initial state we have one probationary customer with an issued 
reservation. From here we start adding reservation attempts and watch for 
data inconsistencies that alert us to fraud *)
Init == 
    /\ pc = ""
        
Next == 
    \/ (pc = "done" /\ UNCHANGED << pc >>)
    \/ onMailBounceCallback
    \/ onDisputeCallback
    \/ onAttemptForTrusted
    \/ onAttemptForProbationary
    \/ onAttemptForFraud
    

Spec == Init /\ [][Next]_<< attempt, customerHistory, pc >>


=============================================================================
\* Modification History
\* Last modified Mon Oct 23 08:16:53 CST 2017 by ASUS
\* Created Tue Oct 03 13:24:03 CST 2017 by ASUS
