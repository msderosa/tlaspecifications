------------------------------ MODULE FollowMe ------------------------------
EXTENDS Integers, Area, TLC

CONSTANTS totalSvcArea
(*
This specification details the effects of
1. turning on / off follow mode 
2. rotating the phone between portrait / landscape 
on the positioning of the map
*)

(*
The state consists of:
1. the ui follow mode setting and the phone orientation
2. a camera that views a section map, which extends from -18 degreees to 18 
degrees longitude
*)
VARIABLES ui, camera

maxWidth ==
    IF ui.orientation = "portrait"
        THEN 8
        ELSE 16
        
(*
we cant zoom out any more - increment(area) when the current viewport
is 7, 8 in portrait or 15, 16 in landscape
*)        
fullyZoomedOut(cam) == 
    width(cam) >= maxWidth - 1
        
RECURSIVE repositionByOrgRules(_)
repositionByOrgRules(cam) ==
    IF intersects(totalSvcArea, cam) THEN
        cam
    ELSE IF /\ fullyZoomedOut(cam)
             /\ \lnot intersects(totalSvcArea, cam) THEN
        totalSvcArea   \* a well defined svcArea, like last destination
                       \* totalSvcArea is just a stand in for something 
                       \* more refined, potentially
    ELSE repositionByOrgRules(increment(cam))

\* \E s \in 0..36: \E w \in 1..maxWidth
cameraRandomMove(cam, s, w) == 
    << s - 18, ((s + w) % 36) - 18 >>
    
translateExtentTo(str, cam) == 
    IF str = "portrait"
        THEN decrement(decrement(decrement(decrement(cam))))
        ELSE increment(increment(increment(increment(cam))))

----------------------------------------------------------------    

TypeInv == 
    /\ ui \in [followMe: BOOLEAN,
               orientation: {"portrait", "landscape"}]
    /\ camera \in [syncedToMap: BOOLEAN,
                   sw: -18..18,
                   ne: -18..18]

(* The camera viewport can show any position over the lat/lng range of 
the map but it must have a width and that width must be within the 
screen limits.
*)
CameraLimitsInv == 
    /\ camera.sw /= camera.ne
    /\ IF ui.orientation = "portrait"
        THEN width(camera) <= 8
        ELSE width(camera) <= 16
        
(* If we are not in follow mode then auto positioning should make some part of
the service area visible
*)        
FollowMeOffInv ==
    (ui.followMe = FALSE /\ camera.syncedToMap) => 
        intersects(totalSvcArea, camera)

Inv == /\ TypeInv
       /\ CameraLimitsInv
       /\ FollowMeOffInv

--------------------------------------------------

(* When follow mode is entered the camera is moved to the (random) position where the
user is and we notify the map to update its display
*)
OnFollowMeOn == 
    /\ (ui.followMe = FALSE /\ camera.syncedToMap = TRUE)
    /\ ui' = [ui EXCEPT !.followMe = TRUE]
    /\ \E n \in 0..36: 
        \E w \in 1..maxWidth: 
            camera' = LET swNe == cameraRandomMove(camera, n, w)
                      IN [syncedToMap |-> FALSE,
                          sw |-> swNe[1],
                          ne |-> swNe[2]]

(* If we turn off follow me and there is something on the map then do nothing.
If the map is not showing anything then we want to do auto assist and
extend out to (a) the second closest city or (b) recentering their last search
with valid bounds
*)
OnFollowMeOff ==
    /\ (ui.followMe = TRUE /\ camera.syncedToMap = TRUE)
    /\ ui' = [ui EXCEPT !.followMe = FALSE]
    /\ IF intersects(totalSvcArea, camera)
        THEN UNCHANGED camera
        ELSE LET bnds == repositionByOrgRules(camera)
              IN camera' = [syncedToMap |-> FALSE,
                            sw |-> bnds.sw,
                            ne |-> bnds.ne]

(* When the phone rotates the width of the view that the camera takes in
will either expand or contract
*)
OnPhoneRotate == 
    /\ camera.syncedToMap = TRUE
    /\ IF ui.orientation = "portrait"
        THEN LET newBnds == translateExtentTo("landscape", camera)
              IN /\ ui' = [ui EXCEPT !.orientation = "landscape"]
                 /\ camera' = [syncedToMap |-> FALSE,
                               sw |-> newBnds.sw,
                               ne |-> newBnds.ne]
        ELSE LET newBnds == translateExtentTo("portrait", camera)
              IN /\ ui' = [ui EXCEPT !.orientation = "portrait"]
                 /\ camera' = [syncedToMap |-> FALSE,
                               sw |-> newBnds.sw,
                               ne |-> newBnds.ne]

OnUserReposition == 
    /\ camera.syncedToMap = TRUE
    /\ \E s \in 0..36: 
        \E w \in 1..maxWidth:
            LET rm == cameraRandomMove(camera, s, w)
            IN camera' = [syncedToMap |-> FALSE,
                          sw |-> rm[1], 
                          ne |-> rm[2]]
    /\ UNCHANGED ui

(* If we are not in follow mode then we display markers or if there are no
markers we try to auto position the map so that we show at least two cities
*)
SyncCameraToMapAutoAssist == 
    /\ (camera.syncedToMap = FALSE /\ ui.followMe = FALSE)
    /\ IF intersects(totalSvcArea, camera)
        THEN camera' = [camera EXCEPT !.syncedToMap = TRUE]
        ELSE LET bnds == repositionByOrgRules(camera)
              IN camera' = [camera EXCEPT !.sw = bnds.sw,
                                           !.ne = bnds.ne]
    /\ UNCHANGED ui

(* In follow mode we just get any markers that are available (or not). We dont
do any auto assist actions
*)
SyncCameraToMapNoAssist == 
    /\ (camera.syncedToMap = FALSE /\ ui.followMe = TRUE)
    /\ camera' = [camera EXCEPT !.syncedToMap = TRUE]
    /\ UNCHANGED ui

----------------------------------------------------------------
Init == 
    /\ ui = [followMe |-> FALSE,
             orientation |-> "portrait"]
    /\ camera = [syncedToMap |-> TRUE,
                 sw |-> -8,
                 ne |-> -7]

Next == \/ OnFollowMeOn
        \/ OnFollowMeOff
        \/ OnUserReposition
        \/ OnPhoneRotate
        \/ SyncCameraToMapAutoAssist
        \/ SyncCameraToMapNoAssist

Spec == Init /\ [][Next]_<< ui, camera >>
=============================================================================
\* Modification History
\* Last modified Sun Nov 16 20:17:04 CST 2014 by marco
\* Created Sat Nov 15 12:45:36 CST 2014 by marco
