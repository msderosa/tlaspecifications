---------------------------- MODULE StateMapInteraction ----------------------------
EXTENDS Integers

VARIABLES state

(*
Im going to represent the map area by 7 unit squares.
The posible map postitions are the three center unit squares and
the possible zooms are in {1,2,3} for the zoom at which we show
cities, nieghborhoods, garages given mode = {Garage}
cities, cities, streets given mode = {Street}
cities, nieghborhoods, garages given mode = {Garage, Street}
indoor, outdoor, valet, self, no cash only, eclectric, clearance,  suv 
*)
MapRange == 0..7
AllFilters == [
               exposure: SUBSET {"Indoor", "Outdoor"} \ {{}},
               agency: SUBSET {"Valet", "Self"} \ {{}},
               payment: SUBSET {"Credit", "Cash"} \ {{}},
               electricCharging: SUBSET BOOLEAN \ {{}},
               suv: BOOLEAN
              ]

MapInvar == /\ state \in [mapZoom : {1, 2, 3},
                          mapArea : MapRange \X MapRange,
                          rateStruct : {"Daily", "Monthly"},
                          prevMapMode : SUBSET {"Garage", "Street"},
                          mapMode : (SUBSET {"Garage", "Street"}) \ {{}},
                          interval: {1, 2},                                \* possible date ranges, only 2 for simplicity
                          filter: AllFilters]
            /\ state.mapArea[1] < state.mapArea[2]
            /\ state.rateStruct = "Monthly" => state.mapMode = {"Garage"}
            /\ state.mapArea[1] > 0 => "Street" \notin state.mapMode
            /\ state.mapMode /= {}

MapInit == /\state = [rateStruct |-> "Daily",
                      mapZoom |-> 3,
                      prevMapMode |-> {},
                      mapMode |-> {"Garage"},
                      mapArea |-> << 3, 4 >>,
                      interval |-> 1,
                      filter |-> [
                            exposure |-> {"Indoor", "Outdoor"},
                            agency |-> {"Valet", "Self"},
                            payment |-> {"Credit", "Cash"},
                            electricCharging |-> {TRUE, FALSE},
                            suv |-> FALSE
                      ]]

MapPos(a) == IF a \in {<<3,4>>, <<2,5>>, <<1,6>>} THEN 0
             ELSE IF a \in {<<2,3>>, <<1,4>>, <<0,5>>} THEN -1
             ELSE 1

\* just for model purposes I am going to assume that the only area
\* that contain streets is the first location
\* in our linear area
AvailableModes(a) == IF a[1] = 0
                     THEN (SUBSET {"Garage", "Street"}) \ {{}}
                     ELSE {{"Garage"}}
ActiveModeCtls(a) == IF a[1] = 0
                     THEN {"Garage", "Street"}
                     ELSE {"Garage"}

OnFilterChg == \E f \in AllFilters : state' = [state EXCEPT !.filter = f]

OnZoomChg == \E n \in 1..3 : 
                LET diff == state.mapZoom - n
                    area == << state.mapArea[1] - diff, state.mapArea[2] + diff>>
                    possibleModes == ActiveModeCtls(area)
                    nextModes == state.mapMode \cap possibleModes
                IN state' = [state EXCEPT !.mapArea = area, 
                                          !.mapZoom = n,
                                          !.mapMode = IF nextModes = {}
                                                      THEN {"Garage"}
                                                      ELSE nextModes]

OnPosChg == LET c == MapPos(state.mapArea)
                areaZ == << state.mapArea[1] - c, state.mapArea[2] - c >>
            IN IF c /= 0
               THEN LET possibleModes == ActiveModeCtls(areaZ)
                        nextModes == state.mapMode \cap possibleModes
                    IN state' = [state EXCEPT !.mapArea = areaZ,
                                              !.mapMode = IF nextModes = {}
                                                          THEN {"Garage"}
                                                          ELSE nextModes]
               ELSE \E n \in {-1, 1} : 
                    LET possibleModes == ActiveModeCtls(<< state.mapArea[1] + n, 
                                                           state.mapArea[2] + n >>)
                        nextModes == state.mapMode \cap possibleModes
                    IN state' = [state EXCEPT !.mapArea = << @[1] + n, @[2] + n >>,
                                              !.mapMode = IF nextModes = {}
                                                          THEN {"Garage"}
                                                          ELSE nextModes]

\* a montly rate structure naturally forces us into a map mode of {Garage}. We will
\* assume that Monthly is incompatable with both mode {Street}, {Garage, Street} 
OnRateStrChg == IF (/\ state.rateStruct = "Daily" 
                    /\ state.mapMode \in {{"Street"}, {"Garage","Street"}})
                THEN state' = [state EXCEPT !.rateStruct = "Monthly", 
                                            !.mapMode = {"Garage"}]
                ELSE IF state.rateStruct = "Daily"
                THEN state' = [state EXCEPT !.rateStruct = "Monthly"]
                ELSE state' = [state EXCEPT !.rateStruct = "Daily"]

OnModeChg == IF state.rateStruct = "Daily"
             THEN \E m \in AvailableModes(state.mapArea) : 
                state' = [state EXCEPT !.prevMapMode = state.mapMode,
                                       !.mapMode = m]
             ELSE state' = [state EXCEPT !.prevMapMode = state.mapMode,
                                         !.mapMode = {"Garage"}]
             
OnIntChg == /\ state.rateStruct = "Daily"
            /\ IF state.interval = 1
               THEN state' = [state EXCEPT !.interval = 2]
               ELSE state = [state EXCEPT !.interval = 1]
        
MapNext == \/ OnPosChg
           \/ OnZoomChg
           \/ OnRateStrChg
           \/ OnModeChg
           \/ OnIntChg

Spec == MapInit /\ [][MapNext]_<< state >> 


=============================================================================
\* Modification History
\* Last modified Wed Sep 10 19:00:00 CST 2014 by marco
\* Created Sun Aug 17 13:51:38 CST 2014 by marco
    