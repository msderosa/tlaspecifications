---------------------------- MODULE MapRequests ----------------------------
EXTENDS Integers, Sequences, TLC

VARIABLES state, uithread, tokens, overlayStore, counter

vars == << state, uithread, tokens, overlayStore, counter >>

MaxTokens == 3
Ctx == INSTANCE StateMapInteraction WITH state <- state

InCtxGarages(s) == /\ s.mapZoom = 3
                   /\ "Garage" \in s.mapMode
InCtxStreets(s) == /\ s.mapZoom = 3
                   /\ "Street" \in s.mapMode
InCtxNbrHoods(s) == s.mapZoom = 2
InCtxSvcAreas(s) == s.mapZoom = 1

Min(a, b) == IF a < b THEN a ELSE b
Max(a, b) == IF a > b THEN a ELSE b
Expand(a) == << Max(0, a[1] - 1), Min(7, a[2] + 1) >>
Encloses(a, b) == (a[1] <= b[1]) /\ (a[2] >= b[2])
                       
MapModeToType(m) == IF m = {"Garage", "Street"} THEN "GS"
                    ELSE IF m = {"Garage"} THEN "G"
                    ELSE IF m = {"Street"} THEN "S"
                    ELSE "" 

Init == /\ state = [rateStruct |-> "Daily",
                    mapZoom |-> 3,
                    prevMapMode |-> {},
                    mapMode |-> {"Garage"},
                    mapArea |-> << 3, 4 >>,
                    interval |-> 1]
        /\ uithread = "await"
        /\ tokens = << >>
        /\ overlayStore = [counter |-> -1, type |-> "", queryArea |-> << 0, 0 >>]
        /\ counter = 0

MrInvar == /\ Ctx!MapInvar
           /\ uithread \in {"await", "cameraChg", "dateChg", "modeChg", "rateChg"}
           /\ Len(tokens) \in 0..(MaxTokens + 1)
           /\ overlayStore \in [counter: Int, 
                                type: {"G", "S", "N", "CA", ""}, 
                                queryArea: Int \X Int]
           /\ (overlayStore.counter = -1 \/ overlayStore.counter > 0)
           /\ (/\ overlayStore.counter = counter 
               /\ uithread = "await") => Encloses(overlayStore.queryArea, state.mapArea)
           /\ (/\ overlayStore.counter = counter 
               /\ state.mapZoom = 3
               /\ uithread = "await") => overlayStore.type = MapModeToType(state.mapMode)

OnCameraUpdate == /\ uithread = "await" /\ Len(tokens) <= MaxTokens
                  /\ \/ Ctx!OnZoomChg
                     \/ Ctx!OnPosChg
                  /\ uithread' = "cameraChg"
                  /\ UNCHANGED << tokens, overlayStore, counter >>
OnRateStructChg == /\ uithread = "await" /\ Len(tokens) <= MaxTokens /\ counter > 0
                   /\ Ctx!OnRateStrChg
                   /\ uithread' = "rateChg"
                   /\ UNCHANGED << tokens, overlayStore, counter >>
OnMapModeChg == /\ uithread = "await" /\ Len(tokens) <= MaxTokens /\ counter > 0
                /\ Ctx!OnModeChg
                /\ uithread' = "modeChg"
                /\ UNCHANGED << tokens, overlayStore, counter >>
OnDateChg == /\ uithread = "await" /\ Len(tokens) <= MaxTokens /\ counter > 0
             /\ Ctx!OnIntChg
             /\ uithread' = "dateChg"
             /\ UNCHANGED << tokens, overlayStore, counter >> 

(*
Give a particular parking context and a particular overlayStore. We should conside 
the execution of the ReqOn... functions below in the context of a group of 
service areas being prepared for a request. The possible groupings are groupings for
(a) service area markers (b) neighborhood markers (c) garage markers, or (d)
segment polylines. A token' update then should be interpreted as the actual
release of a request.
*)

(*
This change affects svcAreas showing sa | nbrhd | garage | steet overlays
If we dont know the contents of the overlayStore then we release the request,
othewise we check if the service area exists and it contents are identical to
what we plan to fetch. If it is we skip any requests.
If the service area doesnt exist or it has incorrect contents then we send off
a request.
*)
ReqOnCameraUpdate == 
                 /\ uithread = "cameraChg"
                 /\ IF overlayStore.counter = counter
                    THEN IF ( /\ Encloses(overlayStore.queryArea, state.mapArea)
                              /\ overlayStore.type = MapModeToType(state.mapMode) )
                         THEN /\ uithread' = "await"
                              /\ UNCHANGED << state, overlayStore, counter, tokens >>
                         ELSE /\ tokens' = Append(tokens, [type |-> MapModeToType(state.mapMode), 
                                                           counter |-> counter + 1, 
                                                           queryArea |-> Expand(state.mapArea)])
                              /\ counter' = counter + 1
                              /\ uithread' = "await"
                              /\ UNCHANGED << state, overlayStore >>
                    ELSE /\ tokens' = Append(tokens, [type |-> MapModeToType(state.mapMode), 
                                                      counter |-> counter + 1, 
                                                      queryArea |-> Expand(state.mapArea)])
                         /\ counter' = counter + 1
                         /\ uithread' = "await"
                         /\ UNCHANGED << state, overlayStore >>
(*
Corresponds routine will be called after a garage | street | all map mode change.
This change affects svcAreas that are showing streets | garages
If we dont know the contents of the overlayStore then we release the request. Otherwise
we have a situation where either we need to make a garage | street request, or we
have to delete garage | street overlays from the map.
Note: we are assumeing mode changes follow the behavoir
G <-> GS <-> S  
*)
ReqOnMapModeChange == 
          /\ uithread = "modeChg"
          /\ IF( InCtxStreets(state) \/ InCtxGarages(state) )
             THEN IF overlayStore.counter = counter
                  THEN IF state.mapMode = {"Garage", "Street"}  
                 \* update just 1 of the overlays
                       THEN /\ tokens' = Append(tokens, [type |-> MapModeToType(state.mapMode), 
                                                         counter |-> counter + 1, 
                                                         queryArea |-> Expand(state.mapArea)])
                            /\ counter' = counter + 1
                            /\ uithread' = "await"
                            /\ UNCHANGED << state, overlayStore >>
                  \* clear one of the overlays
                       ELSE /\ overlayStore' = [type |-> MapModeToType(state.mapMode), 
                                                counter |-> counter + 1, 
                                                queryArea |-> Expand(state.mapArea)]
                            /\ counter' = counter + 1
                            /\ uithread' = "await"
                            /\ UNCHANGED << state, tokens >>
                  ELSE /\ tokens' = Append(tokens, [type |-> MapModeToType(state.mapMode), 
                                                    counter |-> counter + 1, 
                                                    queryArea |-> Expand(state.mapArea)])
                       /\ counter' = counter + 1
                       /\ uithread' = "await"
                       /\ UNCHANGED << state, overlayStore >>
             ELSE UNCHANGED vars
(*
This routing is called after a change in the daily | monthly setting.
This change only affects svcAreas that are showing streets | garages overlays
*)
ReqOnRateStrucChange == 
                /\ uithread = "rateChg"
                /\ IF ( InCtxStreets(state) \/ InCtxGarages(state) )
                   THEN /\ tokens' = Append(tokens, [type |-> MapModeToType(state.mapMode), 
                                                     counter |-> counter + 1, 
                                                     queryArea |-> Expand(state.mapArea)])
                        /\ counter' = counter + 1
                        /\ uithread' = "await"
                        /\ UNCHANGED << state, overlayStore >>
                   ELSE UNCHANGED vars

(* The context calculation in real code has to be done relative to
This routine is called after any data change.
This change affects svcAreas that are showing streets | garages overlays
*)
ReqOnDateChange == 
           /\ uithread = "dateChg"
           /\ IF ( InCtxStreets(state) \/ InCtxGarages(state) )
              THEN /\ tokens' = Append(tokens, [type |-> MapModeToType(state.mapMode), 
                                                counter |-> counter + 1, 
                                                queryArea |-> Expand(state.mapArea)])
                   /\ counter' = counter + 1
                   /\ uithread' = "await"
                   /\ UNCHANGED << state, overlayStore >>
              ELSE UNCHANGED vars

(*
Here we recieve a response into the overlayStore and remove the appropriate token
from the list. We would also ordinarily create and store our map symbols at this
point
*)
Recieve == /\ Len(tokens) > 0
           /\ LET tk == Head(tokens)
              IN /\ tokens' = Tail(tokens)
                 /\ overlayStore' = tk
                 /\ UNCHANGED << uithread, state, counter >>

Next == \/ OnCameraUpdate
        \/ OnRateStructChg
        \/ OnMapModeChg
        \/ OnDateChg
        \/ ReqOnDateChange
        \/ ReqOnRateStrucChange
        \/ ReqOnCameraUpdate
        \/ ReqOnMapModeChange
        \/ Recieve

Spec == Init /\ [][Next]_vars
=============================================================================
\* Modification History
\* Last modified Wed Aug 27 19:28:46 CST 2014 by marco
\* Created Wed Aug 20 20:56:28 CST 2014 by marco
