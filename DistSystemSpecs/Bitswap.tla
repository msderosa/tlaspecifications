------------------------------ MODULE Bitswap ------------------------------
EXTENDS IpfsNode, IpfsMsg, TLC

(*
The state of the system consists of the state of the individual nodes and the
state of the communication medium.
(a) The internal state includes the current connections, a ledger for each
connection, messages to send and recieve, and the nodes active / inactive state.
(b) The state of the communication medium is the collection of all the messages 
which are in transit on the network.
*)
VARIABLES nodes,       \* internal state of the nodes 
    medium             \* communication medium state

TypeInv == 
    /\ nodes \in [ Nodes ->  NodeStates]
    /\ \A nd \in Nodes: nd \notin nodes[nd].openCnns
    /\ medium \in SUBSET Msgs
    /\ \A msg \in medium: PropValidMsgFormats(msg)

\* A node makes a request to an external node to open a connection
OpenSend(nd) == 
    LET nState == nodes[nd]
    IN /\ nState.networkState = "online"
       /\ \E ndd \in (Nodes \ nState.openCnns):
               /\ nd /= ndd
               /\ medium' = medium \cup { [op |-> "openInit",
                                           from |-> nd,
                                           to |-> ndd,
                                           data |-> << nState.credentials >>] }
       /\ UNCHANGED nodes 

(* Here a message in the open series comes off the network; the steps 
in the open series are:
 0 --> openInit <<creds>>
 1 <-- openAccept <<creds>> | openReject
 ? --> openReject
 The openReject message is not strictly needed as it does not change any state
 but it does have informational value for the requesting node and allows 
 distinguishing from the situation where connectivity fails.
*)
OpenRecv(nd, msg) ==
    LET nState == nodes[nd]
    IN IF nState.networkState = "online" THEN
        CASE (msg.op = "openInit" /\ msg.data[1] = "valid") ->
                  /\ nodes' = [ nodes EXCEPT ![nd] = 
                                    [@ EXCEPT !.openCnns = @
                                                            \cup
                                                            {msg.from}] ]
                  /\ medium' = (medium \ {msg}) \cup
                                  { [op |-> "openAccept", from |-> nd, to |-> msg.from, 
                                      data |-> << nState.credentials >>] }
        [] (msg.op = "openInit" /\ msg.data[1] = "invalid") ->
                  /\ medium' = (medium \ {msg}) \cup
                                  { [op |-> "openReject", from |-> nd, to |-> msg.from,
                                      data |-> << >>] }
                  /\ UNCHANGED nodes
        [] (msg.op = "openAccept" /\ msg.data[1] = "valid") ->
                  /\ nodes' = [ nodes EXCEPT ![nd] = 
                                    [@ EXCEPT !.openCnns = @
                                                            \cup
                                                            {msg.from}] ]
                  /\ medium' = medium \ {msg}
        [] (msg.op = "openAccept" /\ msg.data[1] = "invalid") ->
                  /\ medium' = (medium \ {msg}) \cup
                                  { [op |-> "openReject", from |-> nd, to |-> msg.from,
                                      data |-> << >>] }
                  /\ UNCHANGED nodes
        [] msg.op = "openReject" ->
                  /\ medium' = medium \ {msg}
                  /\ UNCHANGED nodes 

    ELSE /\ medium' = medium \ {msg}
          /\ UNCHANGED nodes


Online(nd) == 
    /\ nodes[nd].networkState = "offline"
    /\ nodes' = [ nodes EXCEPT ![nd] =
                    [@ EXCEPT !.networkState = "online"] ]
    /\ UNCHANGED medium

(* When a node goes offline it changes its network state and it clears 
its list of open connections
*)
Offline(nd) == 
    /\ nodes[nd].networkState = "online"
    /\ nodes' = [ nodes EXCEPT ![nd] = 
                    [@ EXCEPT !.networkState = "offline",
                               !.openCnns = {}] ]
    /\ UNCHANGED medium
    
MessageLoss(msg) ==
    /\ medium' = medium \ {msg}
    /\ UNCHANGED nodes

Init == 
    /\ nodes = [ nd \in Nodes |-> [credentials |-> "valid",
                                   networkState |-> "offline",
                                   openCnns |-> {}] ]
    /\ medium = {}

Next == \/ \E nd \in Nodes: \/ OpenSend(nd)
                            \/ Online(nd)
                            \/ Offline(nd)
        \/ \E msg \in medium: \/ MessageLoss(msg)
                              \/ OpenRecv(msg.to, msg)

Spec == Init /\ [][Next]_<< nodes, medium >>

=============================================================================
\* Modification History
\* Last modified Tue Jun 16 18:14:33 CST 2015 by marco
\* Created Sun Jun 14 20:14:50 CST 2015 by marco
