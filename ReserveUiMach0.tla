--------------------------- MODULE ReserveUiMach0 ---------------------------
EXTENDS ReserveUiCtx0, TLC
VARIABLE customer, pc, bResSubmitted

InvType == 
    /\ customer \in Customers
    /\ pc \in ProcessControl
    /\ bResSubmitted \in BOOLEAN
    
InvFraudstersNoFeedback ==
    /\ (pc \in {"DoneOk", "DoneErr"} /\ "Fraudster" = customer.trustDesignation)
            => bResSubmitted
    /\ pc = "DoneFraud" => customer.trustDesignation \in {"Trusted", "Banned"}

------------------------------------------------------------------------------
(* machine events *)

(* Validate the form data. All fields including cvv and avs should be required
    on the client side and valiated as existing before any submissions.
*)
onDataValidate == 
    /\ pc = "PreviewStart"
    /\ \E b \in BOOLEAN :
        IF b = TRUE
        THEN /\ pc' = "DataStepOk"
              /\ UNCHANGED << bResSubmitted, customer >>
        ELSE UNCHANGED << pc, customer, bResSubmitted >>
    
(* Get a payment method nonce. Even though we may know the customer is a
    fraudster or banned at this point still go through the process. Banned
    customers at this step are treated as trusted customers. Fraudsters 
    aquire a method nonce and regardless of success or failure, fraudsters
    will moved to the next step and later "make a reservation" just like normal. 
*)     
onGetPaymentMethodNonce == 
    /\ pc = "DataStepOk"
    /\ \E rsp \in NonceRsp :
        CASE "Fraudster" = customer.trustDesignation ->
            /\ pc' = "NonceStepOk"
        [] OTHER ->
            /\ CASE rsp.success = TRUE -> pc' = "NonceStepOk"
               [] rsp.success = FALSE /\ rsp.error = "AllOther" -> pc' = "DoneErr"
               [] rsp.success = FALSE /\ rsp.error = "AvsOrCvv" -> pc' = "NonceStepFraud"
    /\ UNCHANGED << bResSubmitted, customer >>

(* Record the cvv / avs failure in the backend systems. Any errors reported on
    the client should
    be very generic and only from the get payment nonce step. There should be no 
    mention of fraud validation failure or details. 
*)
onRecordCvvAvsFailure == 
    /\ pc = "NonceStepFraud"
    /\ pc' = "DoneFraud"
    /\ UNCHANGED << bResSubmitted, customer >>
    
(* Submit to the create reservation API. Assuming normal processing, at this
    point banned customers will generally get a failure message, depending on policy, and
    fraudsters will generally get a success message, depending on policy.
*)
onCreate == 
    /\ pc = "NonceStepOk"
    /\ bResSubmitted' = TRUE
    /\ \E b \in BOOLEAN :
        IF b = TRUE 
        THEN pc' = "DoneOk"
        ELSE pc' = "DoneErr"
    /\ UNCHANGED << customer >>

------------------------------------------------------------------------------
Init == 
    /\ \E c \in Customers : customer = c
    /\ pc = "PreviewStart"
    /\ bResSubmitted = FALSE

Next ==
    \/ pc \in {"DoneErr", "DoneOk", "DoneFraud"} /\ UNCHANGED << pc, customer, bResSubmitted >>
    \/ onDataValidate
    \/ onGetPaymentMethodNonce
    \/ onRecordCvvAvsFailure
    \/ onCreate
    
Spec == Init /\ [][Next]_<< customer, pc, bResSubmitted >>

=============================================================================
\* Modification History
\* Last modified Mon Aug 21 18:34:15 CST 2017 by marco
\* Created Mon Aug 21 15:54:39 CST 2017 by marco
