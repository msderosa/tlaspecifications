-------------------------- MODULE TestLeafSetUtils --------------------------
EXTENDS Integers, LeafSetUtils, FiniteSets, TLC

VARIABLES ls

(* tested for constants M = 3 *)

TestGetLSetContent == 
    /\ LET test == [node |-> 1,
                   right |-> {2, 3},
                   left |-> {0, 7}]
       IN {7, 0, 1, 2, 3} = GetLSetContent(test)
       
TestNeighbors == 
    /\ LET test == [node |-> 1,
                   right |-> {2, 3},
                   left |-> {0, 7}]
       IN /\ 2 = RightNeighbor(test)
          /\ 0 = LeftNeighbor(test)
          /\ NoNode = LeftNeighbor([node |-> 1, left |-> {}, right |-> {}])

TestXMost ==
    /\ LET test == [node |-> 1,
                   right |-> {2, 3, 4},
                   left |-> {0, 7}]
       IN /\ 4 = RightMost(test, 1)
          /\ 7 = LeftMost(test, 1)
          /\ 1 = LeftMost([node |-> 1, left |-> {}, right |-> {}], 1)
          
TestCovers == 
    LET test == [node |-> 1,
                 right |-> {2, 3, 4},
                 left |-> {0, 7}]
        test2 == [node |-> 1,
                  right |-> {3, 4},
                  left |-> {7, 6}]
    IN /\ FALSE = Covers(test, 2)
       /\ FALSE = Covers(test, 0)
       /\ TRUE = Covers(test, 1)
       /\ FALSE = Covers(test2, 0)
       /\ TRUE = Covers(test2, 1)
       /\ TRUE = Covers(test2, 2)
       
TestPruneLSet == /\ {3, 4} = PruneLeftLSet({1, 2, 3, 4}, 5)
                 /\ {0, 2} = PruneRightLSet({0, 2, 3}, 7)

Init == ls = [node |-> 5, left |-> {}, right |-> {}]

TypeInv == /\ ls.left \ ls.right = ls.left
           /\ ls.node \notin UNION {ls.left, ls.right}
           /\ Cardinality(ls.left) <= 2 ^ (L - 1)
           /\ Cardinality(ls.right) <= 2 ^ (L - 1)

Next == \E ns \in SUBSET Nodes : /\ ls' = AddToLSet(ns, ls)

Spec == Init /\ [][Next]_<<ls>>
=============================================================================
\* Modification History
\* Last modified Thu Jan 07 18:49:29 CST 2016 by marco
\* Created Mon Jul 20 16:46:10 CST 2015 by marco
