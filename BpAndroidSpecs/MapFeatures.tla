---------------------------- MODULE MapFeatures ----------------------------
EXTENDS Integers, FiniteSets

(* 
This is a untility module that contain definitions for a one dimensional map  which
can be used to model the actual 2D Google maps
*)

\* a simplified 1-D map on length 10

RangeMin == 0
RangeMax == 10
Range == RangeMin..RangeMax
Bounds == {n \in Range \X Range : n[1] < n[2]}

---------------------------------------------
\* utility functions for working with bounds and point

Contains(bnd, pos) == LET low == bnd[1]
                          high == bnd[2]
                      IN pos >= low /\ pos <= high
                      
Intersects(bndA, bndB) == \/ bndA[1] >= bndB[1] /\ bndA[1] < bndB[2]
                          \/ bndA[2] < bndB[2] /\ bndA[2] >= bndB[1]
                          \/ bndA[1] <= bndB[1] /\ bndA[2] >= bndB[2]

Extent(bnd) == bnd[2] - bnd[1]

Expand(pos, delta) == LET low == IF (pos - delta) < RangeMin THEN RangeMin ELSE pos - delta
                          high == IF (pos + delta) > RangeMax THEN RangeMax ELSE pos + delta
                      IN << low, high >>

ZoomBounds(bnds, limitBnds) == {b \in Bounds: /\ \/ (bnds[1] - b[1] = 1 /\ bnds[2] - b[2] = -1)
                                                 \/ (bnds[1] - b[1] = -1 /\ bnds[2] - b[2] = 1)
                                              /\ Intersects(limitBnds, b) }
TranBounds(bnds, limitBnds) == {b \in Bounds: /\ \/ (bnds[1] - b[1] = 1 /\ bnds[2] - b[2] = 1)
                                                 \/ (bnds[1] - b[1] = -1 /\ bnds[2] - b[2] = -1)
                                              /\ Intersects(limitBnds, b) }
----------------------------------------------

\* simple definitions of service areas and neighborhoods
Nbrhoods == [position: Range]
SvcAreasApt == {sa \in [type: {"A"},
                        ns: {},
                        position: Range,
                        streetParking: {FALSE},
                        bounds: Bounds] : /\ Contains(sa.bounds, sa.position)}
SvcAreasNbhdCty == {sa \in [type: {"C"},
                            ns: SUBSET Nbrhoods,
                            position: Range,
                            streetParking: BOOLEAN,
                            bounds: Bounds] : /\ Contains(sa.bounds, sa.position)
                                              /\ Cardinality(sa.ns) <= 2
                                              /\ \A n \in sa.ns : Contains(sa.bounds, n.position)}
SvcAreasCty == {sa \in [type: {"C"},
                        ns: {},
                        position: Range,
                        streetParking: BOOLEAN,
                        bounds: Bounds] : /\ Contains(sa.bounds, sa.position)}

SvcAreas == SvcAreasApt \union SvcAreasNbhdCty \union SvcAreasCty

-------------------------------------------------

MapModes == {{"Garage"}, {"Street"}, {"Garage", "Street"}}

Maps == [bounds: Bounds, mode: MapModes]
         
=============================================================================
\* Modification History
\* Last modified Thu Oct 09 19:14:51 CST 2014 by marco
\* Created Wed Sep 24 21:49:08 CST 2014 by marco
