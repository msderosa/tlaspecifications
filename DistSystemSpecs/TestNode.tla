------------------------------ MODULE TestNode ------------------------------
EXTENDS Node

UnitTests == 
    LET nd == [id |-> 1,
               leafSet |-> [left |-> {0}, right |-> {2, 3}],
               neighborhood |-> {0, 2, 3},
               routingTable |-> << >>]
        md == [id |-> 1,
               leafSet |-> [left |-> {0}, right |-> {2, 3}],
               neighborhood |-> {0, 1, 2, 3},
               routingTable |-> << >>]
    IN /\ PropNodeNotSelfReferential(nd)
       /\ FALSE = PropNodeNotSelfReferential(md)
       /\ PropDistIsProximity(nd)
       /\ FALSE = PropDistIsProximity(md)

=============================================================================
\* Modification History
\* Last modified Sun Jan 03 14:31:00 CST 2016 by marco
\* Created Thu Jul 23 15:24:19 CST 2015 by marco
