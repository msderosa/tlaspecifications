r---------------------------- MODULE LeafSetUtils ----------------------------
EXTENDS Integers, Ring, Node, FiniteSets

GetLSetContent(lset) == lset.left \cup lset.right \cup {lset.node}

RECURSIVE PruneLeftLSet(_, _)
PruneLeftLSet(ls, node) == 
    IF Cardinality(ls) <= L
    THEN ls
    ELSE LET maxLeft == CHOOSE m \in ls: \A n \in ls: CwDist(node, m) >= CwDist(node, n)
          IN PruneLeftLSet(ls \ {maxLeft}, node)
          
RECURSIVE PruneRightLSet(_, _)
PruneRightLSet(ls, node) == 
    IF Cardinality(ls) <= L
    THEN ls
    ELSE LET maxRight == CHOOSE m \in ls: \A n \in ls: CcwDist(node, m) <= CcwDist(node, n)
          IN PruneRightLSet(ls \ {maxRight}, node)

InnerAddToLSet(ns, lset) == 
    LET rmax == (lset.node + 2 ^ (M - 1)) % (2 ^ M)
        vs == ns \ {lset.node}
    IN IF lset.node < rmax
       THEN LET rs == {n \in vs : /\ n > lset.node /\ n <= rmax}
             IN [lset EXCEPT !.right = @ \cup rs,
                              !.left = @ \cup (vs \ rs)]
       ELSE LET ls == {n \in vs : /\ n < lset.node /\ n > rmax}
             IN [lset EXCEPT !.right = @ \cup (vs \ ls),
                              !.left = @ \cup ls]
(* We add to the right or left of the leaf set depending on which
half of the ring (from the nodes perspective) each new node falls
into. If a new node is exactly opposite the current node on the ring 
then it gets added to the right hand leaf set.
The combined size of the leaf set, left + right = L
*)
AddToLSet(ns, lset) ==
    LET ls == InnerAddToLSet(ns, lset)
    IN [node |-> lset.node,
        right |-> PruneRightLSet(ls.right, ls.node),
        left |-> PruneLeftLSet(ls.left, ls.node)]

(* The closest node in the left leaf set *)
LeftNeighbor(lset) == 
    IF lset.left = {} THEN NoNode
    ELSE CHOOSE m \in lset.left : 
        \A n \in lset.left : CwDist(lset.node, m) <= CwDist(lset.node, n) 
        
RightNeighbor(lset) ==
    IF lset.right = {} THEN NoNode
    ELSE CHOOSE m \in lset.right :
        \A n \in lset.right : CcwDist(lset.node, m) >= CcwDist(lset.node, n) 
        
(* the furthest node in the left leaf set *)        
LeftMost(lset, node) == 
    IF lset.left = {} THEN node
    ELSE CHOOSE m \in lset.left: 
        \A n \in lset.left: CwDist(node, m) >= CwDist(node, n) 
(* the furthest node in the left leaf set *)        
LeftMostEx(lset) == 
    IF lset.left = {} THEN lset.node
    ELSE CHOOSE m \in lset.left: 
        \A n \in lset.left: CwDist(lset.node, m) >= CwDist(lset.node, n)         

RightMost(lset, node) == 
    IF lset.right = {} THEN node
    ELSE CHOOSE m \in lset.right:
        \A n \in lset.right: CcwDist(node, m) <= CcwDist(node, n) 
RightMostEx(lset) == 
    IF lset.right = {} THEN lset.node
    ELSE CHOOSE m \in lset.right:
        \A n \in lset.right: CcwDist(lset.node, m) <= CcwDist(lset.node, n)
  
(* simply a distance measure which spans twice the one side coverage space of a node *)
LeftCover(lset, node) == CwDist(node, LeftNeighbor(lset))

RightCover(lset, node) == CwDist(RightNeighbor(lset), node)

(* a node only covers (holds files for) hashes that are halfway between its own
and its neighbors key values. there should always be neighbors when a node is in
the "ready" state. A node binds more strongly to nodes on its right hand side
that breaks ties when a coverage node falls equally between two possible nodes
*)
Covers(lset, k) == 
    LET rightNbr == RightNeighbor(lset)
        dist == CwDist(rightNbr, k)
    IN /\ 2 * dist >= RightCover(lset, lset.node)
       /\ 2 * dist < 2 * CwDist(rightNbr, lset.node) + LeftCover(lset, lset.node)

(* leaf set constructor *)       
EmptyLS(i) == [node |-> i, left |-> {}, right |-> {}]

(* takes a set of node numbers and a leafset and removes the set  of nodes *)
RemoveFromLSet(ns, lset) == [
    node |-> lset.node,
    left |-> lset.left \ ns,
    right |-> lset.right \ ns]
       
(* true when the left and right leaf sets overlap. this might happen, depending
on the algorithm is the total size of the ring is less than 2L *)
Overlaps(lset) == TRUE

IsComplete(lset) == TRUE

=============================================================================
\* Modification History
\* Last modified Sun Jan 10 22:02:05 CST 2016 by marco
\* Created Sat Jul 18 19:18:23 CST 2015 by marco
