------------------------------ MODULE MSPastry ------------------------------
(* A model of the Pastry distributed hash table. This spec loosely follows the 
approach of the paper, Towards Verification of the Pastry Protocaol using
TLA+, with changes for my own investigations.
*)
EXTENDS Actions 

vars == << status, network, rtable, lset, probing, failed, lease >>

(* A lookup message for a particular key should be answered by one and only
one node, covering the key 
*)
\*CorrectKeyDelivery == \A k \in Keys: \A na, nb \in Nodes: 
\*                            LET lsetA == lset[na]
\*                                lsetB == lset[nb]
\*                            IN 
                            

Init == 
   /\ network = {}
   /\ failed = [n \in Nodes |-> {}]             \* the relation of nodes to all nodes that it has
                                                \* not been able to connect to
   /\ status = [n \in Nodes |-> IF n \in StartNodes THEN "ready"
                                ELSE "dead"]   \* the relation of nodes to their status
   /\ rtable = [n \in Nodes |-> {}]             \* the overlay routing table
   /\ lset = [n \in Nodes |-> {}]               \* a nodes leaf set
   /\ probing = [n \in Nodes |-> {}]
   /\ lease = [na \in Nodes |-> [from \in Nodes |-> 0]]  \* there are no leases in the system to start
   

Next == \E i, j \in Nodes: \/ Deliver(i, j)
                           \/ Join(i, j)
                           \/ Lookup(i, j)
                           \/ RouteLookup(i,j)
                           \/ RouteJoinRequest(i, j)
                           \/ ReceiveJoinRequest(i)
\*                           \/ Probe(i, j)
                           \/ ReceiveJoinReply(i)
                           \/ ReceiveLSProbe(i)
                           \/ ReceiveLSPrRpl(i)
                           \/ SuspectFaulty(i, j)
                           \/ ProbeTimeOut(i, j)
                           \/ RequestLease(i)
                           \/ ReceiveLReq(i)
                           \/ ReceiveBLS(i)
                           \/ LeaseExpired(i, j)
                           \/ DeclareDead(i, j)
                           \/ ResignNode(i)
                           \/ Recover(i)
                           \/ LSRepair(i)
                           \/ MsgLost
                           
Spec == Init /\ [][Next]_vars 

=============================================================================
\* Modification History
\* Last modified Mon Jul 20 16:59:43 CST 2015 by marco
\* Created Sat Jul 18 17:33:06 CST 2015 by marco
