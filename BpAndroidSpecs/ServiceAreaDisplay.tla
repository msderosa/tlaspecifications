---------------------------- MODULE ServiceAreaDisplay ----------------------------
EXTENDS Integers, TLC

(*
This spec describes the changes in a service area sub store as it recieves
updates in the form of http responses. We assume that the service area is unchanging
and for maximum generality we assume that the service area supports Garage and
Street symbol display.
The structure here would, in an application, be part of a large structure that 
contains all the service areas currently on display
*)

\* the two types of service areas are city and airport which we will denote by
\* C and A
\*CONSTANTS svcAreaType

VARIABLES sadState

\* pick some values for some map symbols that will allow us to do checks
markersC == SUBSET {1}
markersA == SUBSET {2}
markersN == SUBSET {11, 12, 13}
markersG == SUBSET {21, 22, 23}
polylines == SUBSET {31, 32, 33} 

MapModes == {"Street", "Garage", "All"}
OverlayTypes == {"Nothing", "SvcAreas", "Nbrhoods", "Garages", "Segments"}

\* Our service area will be either an airport or a city service area and will never
\* change during the lifetime of the sadState object
Init == \E t \in {"A", "C"} : sadState = [
                mkrContents |-> "Nothing",
                markers |-> {},
                plnContents |-> "Nothing",
                polylines |-> {},
                svcArea |-> t]

\* The markers field may contain city, airport, neighborhood, or garage markers
\* The polylines field only contains street segment ploylines. At any point in
\* time the container should then only be showing service areas, neighborhoods or
\* parking infomation (either garages, segments, or both)
TypeInvariant == /\ IF sadState.svcArea = "A"
                    THEN /\ \/ sadState.markers \in markersA
                            \/ sadState.markers \in markersG
                         /\ sadState.polylines = {}
                    ELSE /\ sadState.svcArea = "C"
                         /\ \/ sadState.markers \in markersC
                            \/ sadState.markers \in markersN
                            \/ sadState.markers \in markersG
                         /\ sadState.polylines \in polylines
                 /\ sadState.mkrContents \in {"Nothing", "SvcAreas", "Nbrhoods", "Garages" }
                 /\ sadState.plnContents \in {"Nothing", "Segments"}
                 /\ sadState.mkrContents = "Nothing" => sadState.markers = {}
                 /\ sadState.plnContents = "Nothing" => sadState.polylines = {}

\* Note, that we only care about the map mode at the time the data was requested,  
\* and, for better or for worse, we display what comes through. This decision is 
\* driven by the recognition that
\* (a) we should always have something on the screen even when that data may not 
\* be in sync with current ui selections. And
\* (b) we have no guarantee that any request initiated by a ui change will result 
\* in a recieved response
\* Also note that regardless of the map mode we always request and respond to
\* service area and neighborhood requests
MergeSummaryMarkers(ms, olyType, mapMode) ==
        /\ sadState' = [sadState EXCEPT !.markers = ms, 
                                        !.mkrContents = olyType,
                                        !.plnContents = "Nothing",
                                        !.polylines = {}]
MergeMarkers(ms, olyType, mapMode) == 
        /\ Assert(mapMode /= "Street", "bad garage marker map mode")
        /\ IF mapMode = "Garage"
           THEN sadState' = [sadState EXCEPT !.markers = ms, 
                                             !.mkrContents = olyType,
                                             !.plnContents = "Nothing",
                                             !.polylines = {}]
           ELSE sadState' = [sadState EXCEPT !.markers = ms,
                                             !.mkrContents = olyType]

MergePolylines(ps, mapMode) == 
        /\ Assert(mapMode /= "Garage", "bad street line map mode")
        /\ sadState' = IF mapMode = "Street"   
                       THEN [sadState EXCEPT !.markers = {}, 
                                             !.mkrContents = "Nothing",
                                             !.polylines = ps,
                                             !.plnContents = "Segments"]
                       ELSE [sadState EXCEPT !.polylines = ps,
                                             !.plnContents = "Segments"]

Next == IF sadState.svcArea = "A"
        THEN \/ \E ms \in markersA: \E mm \in MapModes: 
                    MergeSummaryMarkers(ms, "SvcAreas", mm)
             \/ \E ms \in markersG: \E mm \in {"Garage", "All"}:
                    MergeMarkers(ms, "Garages", mm)
        ELSE \/ \E ms \in markersC: \E mm \in MapModes: 
                    MergeSummaryMarkers(ms, "SvcAreas", mm)
             \/ \E ms \in markersN: \E mm \in MapModes:
                    MergeSummaryMarkers(ms, "Nbrhoods", mm)
             \/ \E ms \in markersG : \E mm \in {"Garage", "All"}:
                    MergeMarkers(ms, "Garages", mm)
             \/ \E ps \in polylines : \E mm \in {"Street", "All"}:
                    MergePolylines(ps, mm)

Spec == Init /\ [][Next]_<< sadState >>

=============================================================================
\* Modification History
\* Last modified Thu Oct 09 19:20:31 CST 2014 by marco
\* Created Tue Jul 29 18:20:33 CST 2014 by marco
