-------------------------------- MODULE Node --------------------------------
EXTENDS Integers, Ring, RoutingTable
CONSTANTS L     \* the target size of the leaf set, left + right

(* A collection of nodes that satisfy an abstract, distance measure, 
and that have the symmetry property that len(left) ~ len(right)
*)
LeafSet == [node: Nodes,
            left: SUBSET Nodes,
            right: SUBSET Nodes]

(* TheNeighborhood contains nodes that satisfy a real world, proximity measure.
To reduce the extent of the state space I assume that the distance measure for
the leaf set = the proximity measure, or to put it differently, leafSet == 
neighborhood
*)
NodeState == [id: Nodes,
             leafSet: LeafSet,
             neighborhood: SUBSET Nodes,
             routingTable: RoutingTable]
-----------------------------------------------------------------------------
\* functions

-----------------------------------------------------------------------------
\* node properties which are expected

PropNodeNotSelfReferential(nd) == 
    /\ nd.id \notin (nd.leafSet.left \cup nd.leafSet.right)
    /\ nd.id \notin nd.neighborhood

PropDistIsProximity(nd) == 
    nd.neighborhood = (nd.leafSet.left \cup nd.leafSet.right)

=============================================================================
\* Modification History
\* Last modified Sun Jan 10 21:34:36 CST 2016 by marco
\* Created Thu Jul 23 14:25:38 CST 2015 by marco
