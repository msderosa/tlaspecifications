# Overview #
TLA specifications and proofs for various customer projects and distributed systems

# Contents #
## For directory / ##
Specifications for the BP reservations system

## For directory DistSystemSpecs/ ##
Specifications for the Pastry distributed system protocol

## Fro directory BpAndroidSpecs/ ##
Specifications for the BP Android application, with the pdf specification documents
