---------------------------- MODULE ResCreateMch ----------------------------
EXTENDS Integers, TLC, ResCreateCtx
VARIABLES 
    stepsAccomplished,
    trustCategory,      (* the final trust decision made on the basis of braintree and local data *)
    paymentResult,      (* the detail of acceptance or rejection of charges at braintree *)
    reservationStatus,
    cummulativeSuccess  (* the cummulative success / failure, disposition at each processing step *)

InvType ==
    /\ stepsAccomplished \in ResultSteps
    /\ trustCategory \in TrustCategory 
    /\ paymentResult \in PaymentResult
    /\ reservationStatus \in ReservationStatus
    /\ cummulativeSuccess \in BOOLEAN
    
(* Fraud invariants
All reservation attempts are recorded to the queue table for detail 
    fraud processing. 
All reservation attempts whose trust category is Fraud must succeed from
    the users perspective.    
All reservation attempts whose trust category is Trusted succeed or fail 
    (cvv or avs failures) as the braintree processing succeeds or fails
All reservation attempts whose trust category is Truested succeed or fail
    (fraud failure) as the braintree processing succeeds or fails
*)
InvFraud == 
    /\ ("Queue" \in stepsAccomplished /\ trustCategory = "Fraud") => cummulativeSuccess = TRUE
    /\ ("Queue" \in stepsAccomplished 
        /\ paymentResult \in {"ErrCvvAvs", "Fraud"} 
        /\ trustCategory = "Trusted") => cummulativeSuccess = FALSE

(* Processing invariants
A reservation that results in processing fault at the booking or persist
    steps must rollback payment or booking as applicable 
A reservation that is made but associated with fraud must have a reservation status
    of fraud    
*)
InvProc == 
    /\ (paymentResult = "Ok" /\ cummulativeSuccess = TRUE) => "Process" \in stepsAccomplished
    /\ ("Queue" \in stepsAccomplished /\ cummulativeSuccess = TRUE) => "Process" \in stepsAccomplished
    /\ ("Queue" \in stepsAccomplished /\ cummulativeSuccess = TRUE) => reservationStatus /= ""
    /\ ("Queue" \in stepsAccomplished /\ cummulativeSuccess = FALSE) => reservationStatus = ""

onValidate ==
    /\ (cummulativeSuccess = TRUE /\ stepsAccomplished = {})
    /\ \E b \in BOOLEAN :
        /\ cummulativeSuccess' = b 
        /\ IF b
           THEN /\ stepsAccomplished' = stepsAccomplished \cup {"Validate"}
                /\ UNCHANGED << trustCategory, paymentResult, reservationStatus >>
            ELSE UNCHANGED << stepsAccomplished, trustCategory, paymentResult, reservationStatus >>

(* Pay, Book, and Persist the reservation. Here we will always run the Pay step regardless of our
    characterization of the customer. We do this because we want to get the card information details
    from Braintree for our attempt information. We always Book for PW reservations as that sends out
    the emails. And then we always persist as that will, for BP reservations allow confirmation pages
    and payment cancellation later on 
*)
onProcess == 
    /\ (cummulativeSuccess = TRUE /\ stepsAccomplished = {"Validate"})
    /\ \E rslt \in PaymentResult : \E trust \in TrustCategory :
        /\ paymentResult' = rslt
        /\ trustCategory' = trust
        /\ CASE trust = "Fraud" -> 
                /\ cummulativeSuccess' = TRUE
                /\ reservationStatus' = "fraud"
                /\ stepsAccomplished' = stepsAccomplished \cup {"Process"}
           [] rslt = "Ok" ->
                /\ cummulativeSuccess' = TRUE
                /\ reservationStatus' = "issued"
                /\ stepsAccomplished' = stepsAccomplished \cup {"Process"}
           [] OTHER ->
                /\ cummulativeSuccess' = FALSE 
                /\ UNCHANGED << stepsAccomplished, reservationStatus >>

(* The reservation has been made. At this point just save attempt infomation to the
    process queue, where we can run thought the detailed fraud analysis offline *)
onFinally == 
    /\ (cummulativeSuccess = FALSE \/ (cummulativeSuccess = TRUE /\ "Process" \in stepsAccomplished))
    /\ stepsAccomplished' = stepsAccomplished \cup {"Queue"}
    /\ UNCHANGED << trustCategory, paymentResult, reservationStatus, cummulativeSuccess >>

Init ==
    /\ stepsAccomplished = {}
    /\ trustCategory = "Null"
    /\ paymentResult = ""
    /\ reservationStatus = ""
    /\ cummulativeSuccess = TRUE

Next == 
    \/ ("Queue" \in stepsAccomplished 
        /\ UNCHANGED << stepsAccomplished, trustCategory, 
                        paymentResult, reservationStatus, 
                        cummulativeSuccess >>)
    \/ onValidate
    \/ onProcess
    \/ onFinally

Spec == Init /\ [][Next]_<< stepsAccomplished, trustCategory, paymentResult, reservationStatus, cummulativeSuccess >>

=============================================================================
\* Modification History
\* Last modified Mon Sep 11 14:39:34 CST 2017 by marco
\* Last modified Thu Aug 31 22:35:24 CST 2017 by ASUS
\* Created Mon Aug 28 21:53:24 CST 2017 by ASUS
