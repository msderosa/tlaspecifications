------------------------- MODULE RequestResolution -------------------------
(*
This specification details how service areas can be associated with one or 
more overlay graphic which they should display.
*)
EXTENDS Integers, FiniteSets, TLC, MapFeatures, Sequences

VARIABLES maps, rslvr, sa, vwport

(* 
We have different map viewport sizes (zoom levels) at which we make the
transition between the different map markers. Airport garages have their
own schedule and City garages have thiers as well. 
*)
AirportGarageTransition == 8   \* the extent at which we nominally trasition apt -> garage markers
CityNeighborhoodTransition == 6  \* the extent at which we nominally transition city -> nbrhd markers
CityGarageTransition == 4      \* the extent as which we nominally transition nbrhd -> garage markers

TypeInv == rslvr \in {{"SA"}, {"N"}, {"G"}, {"S"}, {"G", "S"}, {}}
           
RandomMaps == [i \in 1..20 |-> RandomElement(Maps)]
RandomSvcArea == RandomElement(SvcAreas)

Init == /\ rslvr = {"G"}
        /\ vwport = [ bounds |-> <<4, 6>>,
                      mode |-> {"Garage"} ]
        /\ maps = RandomMaps
        /\ sa = RandomSvcArea

(*
The three functions below take a service area which, given the map viewport,
classify the view as appropriate for showing detail, neighborhood, or 
service area markers, and makes the final decision on how that classification
translates into actual markers. The translation is complicated as not only
the viewport of the map but also the map mode determines what markers can be
shown.
*)
GetDetailMkrSet(svcArea, m) == 
    CASE svcArea.streetParking /\ m.mode = {"Street"} -> {"S"}
    [] svcArea.streetParking /\ m.mode = {"Garage"} -> {"G"}
    [] svcArea.streetParking /\ m.mode = {"Garage", "Street"} -> {"G","S"}
    [] (\lnot svcArea.streetParking) /\ m.mode = {"Street"} -> {}
    [] (\lnot svcArea.streetParking) /\ m.mode = {"Garage"} -> {"G"}
    [] (\lnot svcArea.streetParking) /\ m.mode = {"Garage","Street"} -> {"G"}
    
GetNbhdMkrSet(svcArea, m) ==
    CASE (\lnot svcArea.streetParking) /\ m.mode = {"Street"} -> {}
    [] OTHER -> {"N"}
    
GetSaMkrSet(svcArea, m) == 
    CASE (\lnot svcArea.streetParking) /\ m.mode = {"Street"} -> {}
    [] OTHER -> {"SA"}

(* 
We can resolve a service area into either 
{SA}, {N}, {G}, {S}, or {G, S} requests. Here we separate
the resolution algorithm into methods dealing with the 3 general types
of service areas.
*)                            
ResolveAirport(map) == 
    /\ sa.type = "A" /\ Intersects(sa.bounds, map.bounds)
    /\ IF Extent(map.bounds) <= AirportGarageTransition
        THEN rslvr' = {"G"}
        ELSE IF Extent(map.bounds) > Extent(sa.bounds) * 2
                THEN rslvr' = {"SA"}
                ELSE rslvr' = {"G"}
    /\ maps' = Tail(maps)
    /\ vwport' = map
    /\ UNCHANGED << sa >>

ResolveStdCity(map) == 
    /\ sa.type = "C" /\ sa.ns = {} /\ Intersects(sa.bounds, map.bounds)
    /\ IF Extent(map.bounds) <= CityGarageTransition 
        THEN rslvr' = {"G"}
        ELSE IF \lnot Contains(map.bounds, sa.position)
                THEN rslvr' = GetDetailMkrSet(sa, map)
                ELSE rslvr' = GetSaMkrSet(sa, map)
    /\ maps' = Tail(maps)
    /\ vwport' = map
    /\ UNCHANGED << sa >>

ResolveNbrhdCity(map) == 
    /\ sa.type = "C" /\ sa.ns /= {} /\ Intersects(sa.bounds, map.bounds)
    /\ CASE Extent(map.bounds) <= CityGarageTransition ->
            rslvr' = GetDetailMkrSet(sa, map)
       [] (/\ Extent(map.bounds) > CityGarageTransition 
           /\ Extent(map.bounds) <= CityNeighborhoodTransition) ->
              IF \lnot (\E n \in sa.ns : Contains(map.bounds, n.position))
                 THEN rslvr' = GetDetailMkrSet(sa, map)
                 ELSE rslvr' = GetNbhdMkrSet(sa, map)
       [] OTHER ->
            IF /\ Extent(map.bounds) <= Extent(sa.bounds) * 2  
               /\ (\E n \in sa.ns : Contains(map.bounds, n.position))
                THEN rslvr' = GetNbhdMkrSet(sa, map)
                ELSE IF Contains(map.bounds, sa.position)
                    THEN rslvr' = GetSaMkrSet(sa, map)
                    ELSE rslvr' = {}
    /\ maps' = Tail(maps)
    /\ vwport' = map
    /\ UNCHANGED << sa >>
        
DontShowOnMap(map) == /\ \lnot Intersects(sa.bounds, map.bounds)
                      /\ maps' = Tail(maps)
                      /\ rslvr' = {}
                      /\ vwport' = map
                      /\ UNCHANGED << sa >>

(*
If we resolve an area a having a sevice area or neighborhood since we have local data on 
those areas we should ensure that a marker shows up on the map. If we are resolving
garages or streets we really dont know in advance so just assume that if we are within
the service area limits then we will get something back.
*)
AreVisibleMarkers == 
        CASE rslvr = {} -> TRUE
        [] rslvr = {"SA"} -> 
            Contains(vwport.bounds, sa.position)
        [] rslvr = {"N"} -> 
            \E n \in sa.ns : Contains(vwport.bounds, n.position)
        [] OTHER -> TRUE

PossibleResolutions == IF Intersects(sa.bounds, vwport.bounds)
    THEN 
        CASE vwport.mode = {"Street"} /\ sa.streetParking = FALSE ->
            rslvr = {} \* dont show svcArea on the map at all
        [] vwport.mode = {"Street"} /\ sa.streetParking = TRUE ->
            rslvr \in {{"SA"}, {"N"}, {"S"}}
        [] vwport.mode = {"Garage"} /\ sa.ns = {} ->
            rslvr \in {{"SA"}, {"G"}}
        [] vwport.mode = {"Garage"} /\ sa.ns /= {} ->
            rslvr \in {{"SA"}, {"N"}, {"G"}}
        [] vwport.mode = {"Garage", "Street"} /\ sa.streetParking = FALSE /\ sa.ns = {} ->
            rslvr \in {{"SA"}, {"G"}}
        [] vwport.mode = {"Garage", "Street"} /\ sa.streetParking = FALSE /\ sa.ns /= {} ->
            rslvr \in {{"SA"}, {"N"}, {"G"}}
        [] vwport.mode = {"Garage", "Street"} /\ sa.streetParking = TRUE /\ sa.ns = {} ->
            rslvr \in {{"SA"}, {"G", "S"}}
        [] vwport.mode = {"Garage", "Street"} /\ sa.streetParking = TRUE /\ sa.ns /= {} ->
            rslvr \in {{"SA"}, {"N"}, {"G", "S"}}
    ELSE TRUE

(*
The general idea is that when we have no garage requests then we can check to make sure that
neighborhoods mkrs \union city markers \union apt markers != {} and if it is then we
can zoom out
*)                     
Inv == /\ PrintT("-----------")
       /\ PrintT(rslvr)
       /\ PrintT(vwport)
       /\ PrintT(sa)
       /\ TypeInv
       /\ PrintT(1)
       /\ AreVisibleMarkers
       /\ PrintT(2)
       /\ PossibleResolutions
       
Next == IF Len(maps) > 0
        THEN LET m == Head(maps)
              IN \/ ResolveAirport(m)
                 \/ ResolveStdCity(m)
                 \/ ResolveNbrhdCity(m)
                 \/ DontShowOnMap(m)
        ELSE UNCHANGED << maps, rslvr, sa, vwport >> 
                                   
Spec == Init /\ [][Next]_<< maps, rslvr, sa, vwport >>

=============================================================================
\* Modification History
\* Last modified Thu Oct 23 14:28:47 CST 2014 by marco
\* Created Wed Sep 10 19:02:17 CST 2014 by marco
