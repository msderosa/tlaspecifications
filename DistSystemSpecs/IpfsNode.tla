------------------------------ MODULE IpfsNode ------------------------------
EXTENDS Integers

CONSTANTS Nodes     \* the set of available nodes

NodeStates == [
    \* credentials are valid when nodeId = hash(publicKey) and the nodeId satisfies
    \* the network difficulty criterion
    credentials: {"valid", "invalid"},
    networkState: {"online", "offline"},
    openCnns: SUBSET Nodes
    ]
    
=============================================================================
\* Modification History
\* Last modified Tue Jun 16 16:06:26 CST 2015 by marco
\* Created Sun Jun 14 20:47:04 CST 2015 by marco
