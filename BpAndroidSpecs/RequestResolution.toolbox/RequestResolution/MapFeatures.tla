---------------------------- MODULE MapFeatures ----------------------------
EXTENDS Integers, FiniteSets

(* 
For our map we create a 1 dimensional world that ranges from
x = 1 to x = 50 
*)

RangeMin == 0
RangeMax == 10
Range == RangeMin..RangeMax
Bounds == {n \in Range \X Range : n[1] < n[2]}

---------------------------------------------

Contains(bnd, pos) == LET low == bnd[1]
                          high == bnd[2]
                      IN pos >= low /\ pos <= high
                      
Intersects(bndA, bndB) == \/ bndA[1] >= bndB[1] /\ bndA[1] < bndB[2]
                          \/ bndA[2] < bndB[2] /\ bndA[2] >= bndB[1]
                          \/ bndA[1] <= bndB[1] /\ bndA[2] >= bndB[2]

Extent(bnd) == bnd[2] - bnd[1]

Expand(pos, delta) == LET low == IF (pos - delta) < RangeMin THEN RangeMin ELSE pos - delta
                          high == IF (pos + delta) > RangeMax THEN RangeMax ELSE pos + delta
                      IN << low, high >>

ZoomBounds(bnds, limitBnds) == {b \in Bounds: /\ \/ (bnds[1] - b[1] = 1 /\ bnds[2] - b[2] = -1)
                                                 \/ (bnds[1] - b[1] = -1 /\ bnds[2] - b[2] = 1)
                                              /\ Intersects(limitBnds, b) }
TranBounds(bnds, limitBnds) == {b \in Bounds: /\ \/ (bnds[1] - b[1] = 1 /\ bnds[2] - b[2] = 1)
                                                 \/ (bnds[1] - b[1] = -1 /\ bnds[2] - b[2] = -1)
                                              /\ Intersects(limitBnds, b) }
----------------------------------------------

Nbrhoods == [position: Range]
SvcAreasApt == {sa \in [type: {"A"},
                        ns: {},
                        position: Range,
                        streetParking: {FALSE},
                        bounds: Bounds] : /\ Contains(sa.bounds, sa.position)}
SvcAreasNbhdCty == {sa \in [type: {"C"},
                            ns: SUBSET Nbrhoods,
                            position: Range,
                            streetParking: BOOLEAN,
                            bounds: Bounds] : /\ Contains(sa.bounds, sa.position)
                                              /\ Cardinality(sa.ns) <= 2
                                              /\ \A n \in sa.ns : Contains(sa.bounds, n.position)}
SvcAreasCty == {sa \in [type: {"C"},
                        ns: {},
                        position: Range,
                        streetParking: BOOLEAN,
                        bounds: Bounds] : /\ Contains(sa.bounds, sa.position)}

SvcAreas == SvcAreasApt \union SvcAreasNbhdCty \union SvcAreasCty

-------------------------------------------------

MapModes == {{"Garage"}, {"Street"}, {"Garage", "Street"}}

Maps == [bounds: Bounds, mode: MapModes]
         
=============================================================================
\* Modification History
\* Last modified Sun Oct 05 14:17:15 CST 2014 by marco
\* Created Wed Sep 24 21:49:08 CST 2014 by marco
