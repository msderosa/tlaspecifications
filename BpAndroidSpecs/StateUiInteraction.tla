------------------------- MODULE StateUiInteraction -------------------------
EXTENDS FiniteSets, Integers, TLC
(* 
This module details how and why the header and footer areas of the map screen 
respond to state changes
*)
VARIABLES ui, state, store, eventQ

SvcAreas == 
    {sa \in [type: {"City", "Apt"},
             hasNbrhds: BOOLEAN,
             hasStreet: BOOLEAN,
             offersMlySpecials: BOOLEAN] : sa.type = "Apt" => /\ sa.hasNbrhds = FALSE
                                                              /\ sa.hasStreet = FALSE
                                                              /\ sa.offersMlySpecials = FALSE} 

PossibleCurrSvcArea == {sa \in SUBSET SvcAreas: /\ Cardinality(sa) < 2}

\* extract the current service area or {} if there is no current service area
GetSvcArea(st) == IF st.currSvcArea = {}
                        THEN {}
                        ELSE CHOOSE e \in st.currSvcArea: TRUE

------------------------------------------------------------------

\* The date range must always reflect the rate structure. On the other hand if the rate
\* structure changes to monthly we dont really care what the date is
StateTypeInv == 
    /\ state \in [
                currSvcArea: SUBSET SvcAreas,
                mapMode: {{"Garage"}, {"Street"}, {"Garage", "Street"}},
                rateStruct: {"Daily", "Monthly"},
                followMe: {"On", "Off"},
                dateRange: {"<30d", ">=30d"}
                ] 
    /\ IF state.currSvcArea /= {}
        THEN LET sa == CHOOSE e \in state.currSvcArea: TRUE
              IN IF sa.type = "City"
                    THEN /\ (state.rateStruct = "Daily" => state.dateRange = "<30d")
                          /\ (state.dateRange = "<30d" => state.rateStruct = "Daily")
                          /\ (state.rateStruct = "Monthly" => state.dateRange = ">=30d")
                          /\ (state.dateRange = ">=30d" => state.rateStruct = "Monthly")
                    ELSE state.rateStruct = "Daily"
        ELSE TRUE
    /\ (state.currSvcArea /= {} /\ GetSvcArea(state).hasStreet = FALSE) =>
                ("Street" \notin state.mapMode)
    /\ (state.currSvcArea = {}) => (state.mapMode = {"Garage"})

TypeInv == /\ ui \in [
                btnRateStruct: {"visible", "invisible"}, \* Daily | Monthy
                layRateStruct: {"visible", "invisible"},
                btnDates: {"visible", "invisible"}, 
                btnMapMode: {"visible", "invisible"}, \* Garage | Street
                layDatesModes: {"visible", "gone"},
                btnMlySpecials: {"visible", "gone"},
                actCurrentPos: {"on", "off"},
                actLegend: {"enab", "disab"},
                actList: {"enab", "disab"}, \* controlled by the store and the state
                actFilter: {"enab", "disab"},
                layDates: {"visible", "gone"}, \* controlled by the store
                layMapModes: {"visible", "gone"} \* controlled by the store
              ]
           /\ StateTypeInv
           /\ store \in [holds: {{}, {"SA"}, {"N"}, {"G"}, {"S"}, {"G", "S"}},
                         synced: BOOLEAN]
           /\ Cardinality(state.currSvcArea) <= 1
           /\ Cardinality(state.currSvcArea) = 1 => store.holds \in {
                        {"SA"}, {"N"}, {"G"}, {"S"}, {"G","S"}}
           /\ (state.mapMode = {"Garage"} /\ store.synced) => "S" \notin store.holds
           /\ (state.mapMode = {"Street"} /\ store.synced) => "G" \notin store.holds

           /\ IF ui.btnDates = "invisible" /\ ui.btnMapMode = "invisible"
                THEN ui.layDatesModes = "gone"
                ELSE ui.layDatesModes = "visible"
           /\ eventQ \in SUBSET {"Interval", "RateStruct", "MapMode", "ServiceArea", "Store"}

(* screen invariants controlled by the state
*)

\* If (1) we are just showing streets or (2) if we are showing garages but there are
\* no markers on the map then we should not allow garage list activation
GarageListInv == /\ ( (state.mapMode = {"Street"} /\ eventQ = {}) => (ui.actList = "disab") )
                 /\ ((/\ "G" \notin store.holds 
                      /\ store.synced
                      /\ eventQ = {}) => (ui.actList = "disab") )
                 /\ ((/\ "G" \in store.holds 
                      /\ store.synced
                      /\ eventQ = {}) => ui.actList = "enab" )

\* We only want to show the dates and map mode controls and the rate structure controls
\* when we actually have garage markers or street polylines showing on the map.
NoMarkersNoDatesInv == IF (store.synced = TRUE /\ eventQ = {})
                            THEN IF (store.holds = {} \/ "SA" \in store.holds \/ "N" \in store.holds)
                                      THEN /\ ui.layDates = "gone" 
                                            /\ ui.layMapModes = "gone"
                                            /\ ui.layRateStruct = "invisible"
                                      ELSE /\ ui.layDates = "visible" 
                                            /\ ui.layMapModes = "visible"
                                            /\ ui.layRateStruct = "visible"
                            ELSE TRUE

\* if we dont have a well defined service area then we should assume that we are only showing
\* garages, so we dont need any map mode controls
MapModeInv == 
    IF state.currSvcArea = {}
        THEN (state.rateStruct = "Daily" /\ eventQ = {}) => ui.btnMapMode = "invisible"
        ELSE LET sa == CHOOSE e \in state.currSvcArea: TRUE
              IN (/\state.rateStruct = "Daily" 
                  /\ sa.hasStreet = TRUE 
                  /\ eventQ = {}) => ui.btnMapMode = "visible"
              
\* if we are showing just streets the we can disable the filters function
FilterInv == IF eventQ = {} THEN
                IF state.currSvcArea = {} THEN ui.actFilter = "enab"
                ELSE IF state.mapMode = {"Street"} THEN ui.actFilter = "disab"
                ELSE ui.actFilter = "enab"
             ELSE TRUE
              
\* when a current service area is well defined
SvcAreaDefinedInv == 
    (state.currSvcArea /= {} /\ eventQ = {}) =>
        LET sa == CHOOSE e \in state.currSvcArea: TRUE
        IN /\ sa.type = "Apt" => (ui.btnRateStruct = "invisible" /\ ui.btnMapMode = "invisible")
           /\ sa.type = "City" => ui.btnRateStruct = "visible"
           /\ (sa.hasStreet /\ state.rateStruct = "Daily") => /\ ui.btnMapMode = "visible"
                                                              /\ ui.layDatesModes = "visible"
           /\ (sa.hasStreet /\ state.rateStruct = "Monthly") => ui.btnMapMode = "invisible"
           /\ (\lnot sa.hasStreet) => ui.btnMapMode = "invisible"

SvcAreaUndefInv == 
    (state.currSvcArea = {} /\ eventQ = {}) =>
           (ui.btnRateStruct = "visible" /\ ui.btnMapMode = "invisible" /\ ui.actLegend = "disab")

(* When users are interested in monthly rates they wont be using either the date
or the map mode selectors
*)
MonthlyRatesInv == 
    (state.rateStruct = "Monthly" /\ eventQ = {}) =>
        /\ ui.btnRateStruct = "visible" 
        /\ ui.btnDates = "invisible"
        /\ ui.btnMapMode = "invisible"
        /\ ui.layDatesModes = "gone"

DailyRatesInv == 
    IF state.currSvcArea = {} 
        THEN (state.rateStruct = "Daily" /\ eventQ = {}) => /\ ui.btnRateStruct = "visible" 
                                                             /\ ui.btnDates = "visible"
        ELSE LET sa == GetSvcArea(state)
              IN /\ ((state.rateStruct = "Daily" /\ sa.type = "City" /\ eventQ = {}) =>  
                          (ui.btnRateStruct = "visible" /\ ui.btnDates = "visible" /\ ui.layDatesModes = "visible"))
                 /\ ((state.rateStruct = "Daily" /\ sa.type = "Apt" /\ eventQ = {}) =>
                          (ui.btnRateStruct = "invisible" /\ ui.btnDates = "visible" /\ ui.layDatesModes = "visible"))

FollowMeInv == /\ (state.followMe = "On" /\ eventQ = {}) => ui.actCurrentPos = "on"
               /\ (state.followMe = "Off" /\ eventQ = {}) => ui.actCurrentPos = "off"

MonthlyNoStreetsInv == (state.rateStruct = "Monthly" /\ store.synced = TRUE /\ eventQ = {}) 
        => "S" \notin store.holds

MonthlySpecialsInv == 
    LET sa == GetSvcArea(state)
    IN IF (/\ state.currSvcArea /= {} 
            /\ sa.offersMlySpecials 
            /\ state.rateStruct = "Monthly" 
            /\ "Garage" \in state.mapMode)
        THEN eventQ = {} => ui.btnMlySpecials = "visible"
        ELSE eventQ = {} => ui.btnMlySpecials = "gone"

Inv == 
\*       /\ PrintT(0)
       /\ TypeInv
\*       /\ PrintT(1)
       /\ MonthlyRatesInv
\*       /\ PrintT(2)
       /\ GarageListInv
\*       /\ PrintT(3)
       /\ DailyRatesInv
\*       /\ PrintT(4)
       /\ SvcAreaDefinedInv
\*       /\ PrintT(5)
       /\ SvcAreaUndefInv
\*       /\ PrintT(6)
       /\ FollowMeInv
\*       /\ PrintT(7)
       /\ MapModeInv
\*       /\ PrintT(8)
       /\ MonthlySpecialsInv
\*       /\ PrintT(9)
       /\ MonthlyNoStreetsInv
       /\ NoMarkersNoDatesInv
       /\ FilterInv

--------------------------------------------------------------------
(*
The below change functions represent some common user manipulations
of the screens. The final function represents a data response from the
web.  
*)
HandleChangeMapMode ==
    /\ "MapMode" \in eventQ
    /\ CASE state.mapMode = {"Garage", "Street"} -> 
            /\ ui' = [ui EXCEPT !.actList = "enab", !.actFilter = "enab"]
       [] state.mapMode = {"Garage"} ->
            /\ ui' = [ui EXCEPT !.actList = "enab", !.actFilter = "enab"]
       [] OTHER -> 
            /\ ui' = [ui EXCEPT !.actList = "disab", !.actFilter = "disab"]
    /\ eventQ' = eventQ \ {"MapMode"}
    /\ UNCHANGED << state, store >>

HandleChangeRateStructure == 
    /\ "RateStruct" \in eventQ
    /\ CASE state.rateStruct = "Monthly" /\ state.mapMode = {"Garage"} /\ state.currSvcArea = {} ->
            /\ ui' = [ui EXCEPT !.btnDates = "invisible", 
                                 !.btnMapMode = "invisible", 
                                 !.layDatesModes = "gone",
                                 !.btnMlySpecials = "gone"]
       [] state.rateStruct = "Monthly" /\ state.mapMode = {"Garage"} /\ state.currSvcArea /= {} ->
            LET sa == GetSvcArea(state)
            IN IF sa.offersMlySpecials
                THEN /\ ui' = [ui EXCEPT !.btnDates = "invisible", 
                                           !.btnMapMode = "invisible",
                                           !.layDatesModes = "gone",
                                           !.btnMlySpecials = "visible",
                                           !.actFilter = "enab"]
                ELSE /\ ui' = [ui EXCEPT !.btnDates = "invisible", 
                                           !.btnMapMode = "invisible",
                                           !.layDatesModes = "gone",
                                           !.btnMlySpecials = "gone",
                                           !.actFilter = "enab"]
       [] state.rateStruct = "Daily" /\ state.currSvcArea = {} -> \* chng
            /\ ui' = [ui EXCEPT !.btnDates = "visible", 
                                 !.btnMapMode = "invisible", 
                                 !.layDatesModes = "visible",
                                 !.btnMlySpecials = "gone"]
       [] state.rateStruct = "Daily" /\ state.currSvcArea /= {} ->
            LET sa == GetSvcArea(state)
            IN IF sa.hasStreet
                   THEN /\ ui' = [ui EXCEPT !.btnDates = "visible", 
                                              !.btnMapMode = "visible",
                                              !.layDatesModes = "visible", 
                                              !.btnMlySpecials = "gone"]
                   ELSE /\ ui' = [ui EXCEPT !.btnDates = "visible", 
                                              !.btnMapMode = "invisible", 
                                              !.layDatesModes = "visible",
                                              !.btnMlySpecials = "gone"]
    /\ eventQ' = eventQ \ {"RateStruct"}
    /\ UNCHANGED << state, store >>
    
HandleChangeInterval == 
    /\ "Interval" \in eventQ
    /\ CASE state.rateStruct = "Monthly" /\ state.mapMode = {"Garage"} /\ state.currSvcArea = {} ->
            /\ ui' = [ui EXCEPT !.btnDates = "invisible", 
                                 !.btnMapMode = "invisible", 
                                 !.layDatesModes = "gone",
                                 !.btnMlySpecials = "gone"]
       [] state.rateStruct = "Monthly" /\ state.mapMode = {"Garage"} /\ state.currSvcArea /= {} ->
            LET sa == GetSvcArea(state)
            IN IF sa.offersMlySpecials
                THEN /\ ui' = [ui EXCEPT !.btnDates = "invisible", 
                                           !.btnMapMode = "invisible",
                                           !.layDatesModes = "gone",
                                           !.btnMlySpecials = "visible",
                                           !.actFilter = "enab"]
                ELSE /\ ui' = [ui EXCEPT !.btnDates = "invisible", 
                                           !.btnMapMode = "invisible",
                                           !.layDatesModes = "gone",
                                           !.btnMlySpecials = "gone",
                                           !.actFilter = "enab"]
       [] state.rateStruct = "Daily" /\ state.currSvcArea = {} -> \* chng
            /\ ui' = [ui EXCEPT !.btnDates = "visible", 
                                 !.btnMapMode = "invisible", 
                                 !.layDatesModes = "visible",
                                 !.btnMlySpecials = "gone"]
       [] state.rateStruct = "Daily" /\ state.currSvcArea /= {} ->
            LET sa == GetSvcArea(state)
            IN IF sa.hasStreet
                   THEN /\ ui' = [ui EXCEPT !.btnDates = "visible", 
                                              !.btnMapMode = "visible",
                                              !.layDatesModes = "visible", 
                                              !.btnMlySpecials = "gone"]
                   ELSE /\ ui' = [ui EXCEPT !.btnDates = "visible", 
                                              !.btnMapMode = "invisible", 
                                              !.layDatesModes = "visible",
                                              !.btnMlySpecials = "gone"]
    /\ eventQ' = eventQ \ {"Interval"}
    /\ UNCHANGED << state, store >>    

(* the camera position is moved
*)
HandleChangeSvcArea == 
    /\ "ServiceArea" \in eventQ
    /\ IF state.currSvcArea = {} 
            THEN /\ ui' = [ui EXCEPT !.btnRateStruct = "visible",
                                              !.btnMapMode = "invisible",
                                              !.actLegend = "disab",
                                              !.actFilter = "enab",
                                              !.btnMlySpecials = "gone"]
            ELSE LET sa == CHOOSE e \in state.currSvcArea: TRUE
                  IN CASE sa.type = "Apt" -> 
                            /\ ui' = [ui EXCEPT !.btnRateStruct = "invisible",
                                                 !.btnMapMode = "invisible",
                                                 !.btnDates = "visible",
                                                 !.layDatesModes = "visible",
                                                 !.actLegend = "enab",
                                                 !.actFilter = "enab",
                                                 !.btnMlySpecials = "gone"]
                     [] (sa.type = "City" /\ sa.hasStreet = TRUE /\ state.rateStruct = "Daily") -> 
                            /\ ui' = [ui EXCEPT !.btnRateStruct = "visible",
                                                 !.btnMapMode = "visible",
                                                 !.actLegend = "enab",
                                                 !.btnMlySpecials = "gone"]
                     [] (sa.type = "City" /\ sa.hasStreet = FALSE /\ state.rateStruct = "Daily") -> 
                            /\ ui' = [ui EXCEPT !.btnRateStruct = "visible",
                                                 !.btnMapMode = "invisible",
                                                 !.actLegend = "enab",
                                                 !.actFilter = "enab",
                                                 !.btnMlySpecials = "gone"]
                     [] (state.rateStruct = "Monthly" /\ sa.offersMlySpecials = TRUE) ->
                            /\ ui' = [ui EXCEPT !.btnRateStruct = "visible",
                                                 !.btnMapMode = "invisible",
                                                 !.actLegend = "enab",
                                                 !.actFilter = "enab",
                                                 !.btnMlySpecials = "visible"]
                     [] (state.rateStruct = "Monthly" /\ sa.offersMlySpecials = FALSE) ->
                            /\ ui' = [ui EXCEPT !.btnRateStruct = "visible",
                                                 !.btnMapMode = "invisible",
                                                 !.actLegend = "enab",
                                                 !.actFilter = "enab",
                                                 !.btnMlySpecials = "gone"]
    /\ eventQ' = eventQ \ {"ServiceArea"}
    /\ UNCHANGED << state, store >>

(* Runs after we have made a change to the ui by pressing a button or by moving the
camera position
*)
\* chng
HandleChangeStore ==
    /\ "Store" \in eventQ
    /\ IF "G" \in store.holds THEN ui' = [ui EXCEPT !.actList = "enab",
                                                      !.layDates = "visible",
                                                      !.layMapModes = "visible",
                                                      !.layRateStruct = "visible"]
       ELSE IF "S" \in store.holds THEN ui' = [ui EXCEPT !.actList = "disab",
                                                           !.layDates = "visible",
                                                           !.layMapModes = "visible",
                                                           !.layRateStruct = "visible"]
       ELSE ui' = [ui EXCEPT !.actList = "disab",
                              !.layDates = "gone",
                              !.layMapModes = "gone",
                              !.layRateStruct = "invisible"]
    /\ eventQ' = eventQ \ {"Store"}
    /\ UNCHANGED << state, store >>

StateChangeStore ==
    /\ store.synced = FALSE /\ eventQ = {}
    /\ LET pStores == IF state.currSvcArea = {} 
                        THEN  {{"SA"}, {"N"}, {"G"}, {"S"}, {"G","S"}}
                        ELSE LET sa == CHOOSE e \in state.currSvcArea: TRUE
                             IN CASE sa.type = "Apt" -> {{"SA"}, {"G"}}
                                [] /\ sa.type = "City" 
                                   /\ sa.hasNbrhds = TRUE 
                                   /\ sa.hasStreet = TRUE -> 
                                         {{"SA"}, {"N"}, {"G"}, {"S"}, {"G","S"}}
                                [] /\ sa.type = "City" 
                                   /\ sa.hasNbrhds = TRUE 
                                   /\ sa.hasStreet = FALSE -> 
                                         {{"SA"}, {"N"}, {"G"}}
                                [] /\ sa.type = "City" 
                                   /\ sa.hasNbrhds = FALSE 
                                   /\ sa.hasStreet = TRUE -> 
                                         {{"SA"}, {"G"}, {"S"}, {"G","S"}}
                                [] /\ sa.type = "City" 
                                   /\ sa.hasNbrhds = FALSE 
                                   /\ sa.hasStreet = FALSE -> 
                                         {{"SA"}, {"G"}}
           rslvr == IF "Street" \notin state.mapMode 
                     THEN pStores \ {{"S"}, {"G", "S"}}
                     ELSE IF "Garage" \notin state.mapMode
                            THEN pStores \ {{"G"}, {"G", "S"}}
                            ELSE pStores 
       IN /\ \E stre \in rslvr:
                store' = [store EXCEPT !.holds = stre, !.synced = TRUE]
          /\ eventQ' = eventQ \union {"Store"}
          /\ UNCHANGED << ui, state >>

StateChangeMapMode ==
    /\ ui.btnMapMode = "visible" /\ eventQ = {}
    /\ CASE state.mapMode \in {{"Garage"}, {"Street"}} -> 
            /\ state' = [state EXCEPT !.mapMode = {"Garage", "Street"}]
       [] OTHER -> \E e \in {{"Garage"}, {"Street"}}:
                        IF e = {"Garage"}
                            THEN /\ state' = [state EXCEPT !.mapMode = e]
                            ELSE /\ state' = [state EXCEPT !.mapMode = e]
    /\ eventQ' = eventQ \union {"MapMode"}
    /\ store' = [store EXCEPT !.synced = FALSE]
    /\ UNCHANGED << ui >>

StateChangeRateStructure == 
    /\ ui.btnRateStruct = "visible" /\ eventQ = {}
    /\ CASE state.rateStruct = "Daily" ->
            /\ state' = [state EXCEPT !.rateStruct = "Monthly",
                                       !.dateRange = ">=30d",
                                       !.mapMode = {"Garage"}]
       [] state.rateStruct = "Monthly" /\ state.dateRange = "<30d" ->
            /\ state' = [state EXCEPT !.rateStruct = "Daily"]
       [] state.rateStruct = "Monthly" /\ state.dateRange = ">=30d" ->
            /\ state' = [state EXCEPT !.rateStruct = "Daily",
                                       !.dateRange = "<30d"]
    /\ eventQ' = eventQ \union {"RateStruct"}
    /\ store' = [store EXCEPT !.synced = FALSE]
    /\ UNCHANGED << ui >>

StateChangeInterval == 
    /\ ui.layDates = "visible" /\ ui.btnDates = "visible" /\ eventQ = {}
    /\ IF state.currSvcArea /= {}
        THEN LET sa == CHOOSE e \in state.currSvcArea: TRUE
                  newDt == IF state.dateRange = "<30d" THEN ">=30d" ELSE "<30d"
              IN IF sa.type = "Apt" /\ newDt = "<30d"
                    THEN state' = [state EXCEPT !.dateRange = "<30d"]
                 ELSE IF sa.type = "Apt" /\ newDt = ">=30d"
                    THEN state' = [state EXCEPT !.dateRange = ">=30d"]
                 ELSE IF sa.type = "City" /\ newDt = "<30d"
                    THEN state' = [state EXCEPT !.dateRange = "<30d", !.rateStruct = "Daily"]
                 ELSE state' = [state EXCEPT !.dateRange = ">=30d", 
                                              !.rateStruct = "Monthly",
                                              !.mapMode = {"Garage"}]
        ELSE state' = state
    /\ eventQ' = eventQ \union {"Interval"}
    /\ store' = [store EXCEPT !.synced = FALSE]
    /\ UNCHANGED << ui >>

StateChangeSvcArea == 
    /\ eventQ = {}
    /\ \E setSa \in PossibleCurrSvcArea: 
           IF setSa = {} 
            THEN /\ state' = [state EXCEPT !.currSvcArea = setSa, !.mapMode = {"Garage"}]
            ELSE LET sa == CHOOSE e \in setSa: TRUE
                  IN CASE sa.type = "Apt" -> 
                            /\ state' = [state EXCEPT !.currSvcArea = setSa, 
                                                       !.rateStruct = "Daily", 
                                                       !.mapMode = {"Garage"}]
                     [] (sa.type = "City" /\ sa.hasStreet = TRUE /\ state.rateStruct = "Daily") -> 
                            /\ state' = [state EXCEPT !.currSvcArea = setSa,
                                                       !.dateRange = "<30d"]
                     [] (sa.type = "City" /\ sa.hasStreet = FALSE /\ state.rateStruct = "Daily") -> 
                            /\ state' = [state EXCEPT !.currSvcArea = setSa, 
                                                       !.mapMode = {"Garage"},
                                                       !.dateRange = "<30d"]
                     [] state.rateStruct = "Monthly"  ->
                            /\ state' = [state EXCEPT !.currSvcArea = setSa, 
                                                       !.mapMode = {"Garage"}]
    /\ eventQ' = eventQ \union {"ServiceArea"}
    /\ store' = [store EXCEPT !.synced = FALSE]
    /\ UNCHANGED << ui >>
-------------------------------------------------------------------------------

Init == /\ state = [currSvcArea |-> {[type |-> "City", 
                                      hasNbrhds |-> TRUE, 
                                      hasStreet |-> TRUE,
                                      offersMlySpecials |-> TRUE]},
                    mapMode |-> {"Garage"},
                    rateStruct |-> "Daily",
                    followMe |-> "Off",
                    dateRange |-> "<30d"]
        /\ ui = [btnRateStruct |-> "visible", 
                 layRateStruct |-> "visible",
                 btnDates |-> "visible",
                 btnMapMode |-> "visible",
                 layDatesModes |-> "visible", 
                 btnMlySpecials |-> "gone",
                 actCurrentPos |-> "off",
                 actLegend |-> "enab", 
                 actList |-> "enab",
                 actFilter |-> "enab",
                 layDates |-> "visible",
                 layMapModes |-> "visible"]
        /\ store = [holds |-> {"G"}, synced |-> TRUE]
        /\ eventQ = {}

Next == \/ HandleChangeRateStructure
        \/ HandleChangeSvcArea
        \/ HandleChangeStore
        \/ HandleChangeMapMode
        \/ HandleChangeInterval
        \/ StateChangeRateStructure
        \/ StateChangeSvcArea
        \/ StateChangeStore
        \/ StateChangeMapMode
        \/ StateChangeInterval

Spec == Init /\ [][Next]_<< ui, state, store, eventQ >>

=============================================================================
\* Modification History
\* Last modified Wed Dec 24 14:24:27 CST 2014 by marco
\* Created Thu Oct 09 19:26:59 CST 2014 by marco
