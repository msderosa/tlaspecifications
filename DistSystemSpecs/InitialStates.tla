--------------------------- MODULE InitialStates ---------------------------
EXTENDS Message, Ring

CONSTANTS StartNodes    \* the initial set of nodes that are in the ready 
                         \* state

VARIABLES status, network, rtable, lset, probing, failed, lease

Status == {"dead", "ready", "wait", "ok"}

(*
The number of possible nodes in the system is, the same as the extent of the 
possible key space
*)

TypeInv == /\ network \in SUBSET Message
           /\ failed \in [Int -> SUBSET Int]
           /\ status \in [Int -> Status]

(* Consistency condition: we would like our routing to behave such that for any key that is looked up 
 on the network only one node assumes responsibility for that key. If more than
 one node. \A n, m \in Nodes : n.keyspace \cap m.keyspace = {}. We would also
 like for UNION {n.keyspace : n \in Nodes} = Keyspace. In terms of overlay properties
 this would mean that all nodes have a non overlapping keyspace and the Keyspace
 is completely covered. There are no ommision in the keyspace and no overlaps.
 
 Is this reasonable?
 The property will not hold at every instant in time but it should be possible 
 to repair the network as long as Node death < some rate (deaths per message). And
 as long as nodes can maintain local state so they can defer actions if they 
 detect a need to repair. Some probabilist guarantee 
 P(success) = f(#nodes, death rate, leafset size) may be possible.
 
 How would path failures affect the consistency condition?
 Note that links between the nodes can become unavailable so that bidirectional
 communication is unavailable, or that only one direction communication is available.
 Whole parts of the network can become isolated. 
  *)

=============================================================================
\* Modification History
\* Last modified Tue Jan 19 22:05:43 CST 2016 by marco
\* Created Sat Jul 18 15:38:31 CST 2015 by marco
