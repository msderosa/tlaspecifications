---- MODULE MC ----
EXTENDS ReserveUiMach0, TLC

\* SPECIFICATION definition @modelBehaviorSpec:0
spec_150331168659632000 ==
Spec
----
\* INVARIANT definition @modelCorrectnessInvariants:0
inv_150331168659633000 ==
InvType
----
\* INVARIANT definition @modelCorrectnessInvariants:1
inv_150331168659634000 ==
InvFraudstersNoFeedback
----
=============================================================================
\* Modification History
\* Created Mon Aug 21 18:34:46 CST 2017 by marco
