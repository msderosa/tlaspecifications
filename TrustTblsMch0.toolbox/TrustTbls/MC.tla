---- MODULE MC ----
EXTENDS TrustTblsMch0, TLC

\* SPECIFICATION definition @modelBehaviorSpec:0
spec_150504488871038000 ==
Spec
----
\* INVARIANT definition @modelCorrectnessInvariants:0
inv_150504488872139000 ==
InvType
----
\* INVARIANT definition @modelCorrectnessInvariants:1
inv_150504488873140000 ==
InvSimilarity
----
\* INVARIANT definition @modelCorrectnessInvariants:2
inv_150504488874141000 ==
InvRefund
----
=============================================================================
\* Modification History
\* Created Sun Sep 10 20:01:28 CST 2017 by ASUS
