-------------------------- MODULE RoutingTable --------------------------
EXTENDS Integers, Ring, TLC, Sequences

CONSTANTS b   \* the base 2^b which the node id is represented by for routing
               \* purposes
ASSUME b < M /\ M % b = 0

NumericSymbol == 0..((2 ^ b) - 1)
RingPartitionDepth == 0..((M \div b) - 1)
RoutingTable == [RingPartitionDepth -> [NumericSymbol -> Nodes]]

InitRTable == [row \in  RingPartitionDepth |-> [col \in NumericSymbol |-> NoNode]]

RECURSIVE InnerChangeBasis(_, _, _, _)
InnerChangeBasis(n, radix, seq, places) == 
    LET remdr == n % radix
        quotient == n \div radix
    IN IF places = 0
       THEN seq
       ELSE InnerChangeBasis(quotient, radix, <<remdr>> \o seq, places - 1)

ChangeBasis(n, radix, places) ==
    InnerChangeBasis(n, radix, <<>>, places)
    
RECURSIVE InnerMkRoutingTable(_, _, _, _)
InnerMkRoutingTable(rtable, currentRow, nReduced, n) == 
    LET col == nReduced % (2 ^ b)
        quotient == nReduced \div (2 ^ b)
        nextTable == [rtable EXCEPT ![currentRow][col] = n]
    IN IF currentRow = 0
       THEN nextTable
       ELSE InnerMkRoutingTable(nextTable, currentRow - 1, quotient, n)
       
MkRoutingTable(n) == 
    LET rtable == InitRTable
        startRow == ((M \div b) - 1)
    IN InnerMkRoutingTable(rtable, startRow, n, n)
    
InsertRoutingTable(rtable, newNode, n) ==
    LET nRdx == ChangeBasis(n, 2 ^ b, M \div b)
        newNodeRdx == ChangeBasis(newNode, 2 ^ b, M \div b)
        Diffs == {x \in DOMAIN nRdx : nRdx[x] /= newNodeRdx[x]}
        deltaPos == CHOOSE x \in Diffs : \A y \in Diffs : x <= y
    IN IF rtable[deltaPos - 1][newNodeRdx[deltaPos]] = NoNode
       THEN [rtable EXCEPT ![deltaPos - 1][newNodeRdx[deltaPos]] = newNode]
       ELSE rtable 

(* returns the number of digits, starting from the left, which are the
same in the two entities when both are represented in the standard, search
space basis *)
SharedPrefixLength(n, other) == 
    LET nRdx == ChangeBasis(n, 2 ^ b, M \div b)
        otherRdx == ChangeBasis(other, 2 ^ b, M \div b)
        Diffs == {x \in DOMAIN nRdx : nRdx[x] /= otherRdx[x]}
    IN IF Diffs = {}
        THEN 0
        ELSE CHOOSE x \in Diffs : \A y \in Diffs : x <= y

(* given the routing table of a particular node (rtable[i]) this operator
should return the set of nodes in the table *)
GetRTableContent(rtable) == {rtable[r][c] : r \in DOMAIN rtable, c \in 0..((2 ^ b) -1)}

RECURSIVE InnerAddToTable(_, _, _)
InnerAddToTable(rtable, newNodes, n) == 
    IF newNodes = {} THEN rtable
    ELSE LET x == CHOOSE y \in newNodes : TRUE
              temp == InsertRoutingTable(rtable, x, n)
          IN InnerAddToTable(temp, newNodes \ {x}, n)
          
(* augments info in existing rtable with any nodes that  new_table knows about. The
rtable here must belong to the node n *) 
AddToTable(rtable, new_rtable, n) == 
    LET ns == GetRTableContent(new_rtable)
    IN InnerAddToTable(rtable, ns, n)
    
(* returns the next node in the routing table that is one place closer to matching
k than n is to matching k *)
LookupExact(rtable, n, key) == 
    LET nRdx == ChangeBasis(n, 2 ^ b, M \div b)
        keyRdx == ChangeBasis(key, 2 ^ b, M \div b)
        Diffs == {x \in DOMAIN nRdx : nRdx[x] /= keyRdx[x]}
        deltaPos == CHOOSE x \in Diffs : \A y \in Diffs : x <= y
    IN rtable[deltaPos - 1][keyRdx[deltaPos]]

(* select all nodes in the routing table other than n which could be better
matches to k than n itself *)
LookupFuzzy(rtable, n, key) == 
    LET nRdx == ChangeBasis(n, 2 ^ b, M \div b)
        keyRdx == ChangeBasis(key, 2 ^ b, M \div b)
        Diffs == {x \in DOMAIN nRdx : nRdx[x] /= keyRdx[x]}
        deltaPos == CHOOSE x \in Diffs : \A y \in Diffs : x <= y
    IN {rtable[row][col]:row \in (deltaPos - 1)..((M \div b) - 1), col \in 0..((2 ^ b) - 1)} \ {n}
    
=============================================================================
\* Modification History
\* Last modified Thu Jan 14 22:10:26 CST 2016 by marco
\* Created Thu Jan 07 17:28:23 CST 2016 by marco