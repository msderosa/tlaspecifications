-------------------------------- MODULE Ring --------------------------------
EXTENDS Integers

CONSTANTS M    \* defines the size or order of the problems as 2^M

(* General properties of the ring. Note that the extent of the node
space is the same as the extent of the keys space. This requirement is
for convinience. The only real requirement is that there be a linear
mapping from the node to key space and a distance measure 
*)
MaxNodes == 2 ^ M
Nodes == 0..(MaxNodes - 1)
Keys == 0..(MaxNodes - 1)

NoNode == CHOOSE x : x \notin Nodes

(* a positive distance measure by moving clockwise along the
ring from from to to *)
CwDist(from, to) ==
    LET fromAdjusted == IF from < to THEN from + MaxNodes ELSE from
    IN fromAdjusted - to

(* a negative distance measured by moving counter clockwise along the 
ring from from to to *)
CcwDist(from, to) ==
    LET toAdjusted == IF to < from THEN to + MaxNodes ELSE to
    IN from - toAdjusted

(* The least distance between two points on a circular address space. Here, m,
is the order of the total address space size, 2^m. A negative number represents
a counter clockwise direction; a positive number represents a clockwise
direction. Distance binds more strongly in the positive direction than in
the negative direction so the node directly opposite in the right is always
positive *)
Dist(na, nb) == 
    LET diff == CwDist(na, nb)
    IN IF diff <= 2 ^ (M - 1)
        THEN diff
        ELSE CcwDist(na, nb)

AbsDist(na, nb) == LET dist == Dist(na, nb)
    IN IF dist >= 0 THEN dist
       ELSE -1 * dist

=============================================================================
\* Modification History
\* Last modified Sun Jan 10 22:02:29 CST 2016 by marco
\* Created Sat Jul 18 20:10:36 CST 2015 by marco
