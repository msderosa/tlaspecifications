------------------------------ MODULE TestRing ------------------------------
EXTENDS Integers, Ring
VARIABLES m, n

Ns == 0..15

AbsDistPredicate == AbsDist(m, n) <= 8
LimitedRange == LET d == Dist(m, n)
                         halfOrder == 2 ^ (M - 1)
                    IN /\ d <= halfOrder
                       /\ d >= -1 * halfOrder

CwDistPredicate == LET cwd == CwDist(m, n)
                        max == 2 ^ M
                   IN /\ cwd >= 0
                      /\ cwd < max
                       
Symetry == Dist(m, n) = -1 * Dist(n, m)


SanityChecks == /\ Dist(3, 13) = 6
                /\ Dist(1, 2) = -1
                /\ CwDist(1, 2) = 15

Init == /\ m = 0
        /\ n = 0

Next == \E a, b \in Ns :
    /\ m' = a
    /\ n' = b

=============================================================================
\* Modification History
\* Last modified Sun Jan 03 15:15:42 CST 2016 by marco
\* Created Mon Jul 20 15:01:12 CST 2015 by marco
