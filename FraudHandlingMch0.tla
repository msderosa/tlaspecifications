------------------------- MODULE FraudHandlingMch0 -------------------------
EXTENDS Integers, TLC, FraudHandlingCtx0, FiniteSets
VARIABLES as, customerType

(* Assume that the system sees a series of Attempts which it identifies as comming
from a know fraudster. A known fraud attempt is a an attempt where the card, or device,
or the email is associated with a known fraudster. 
*)
ProcessInvariant == 
    /\ \A a \in as : a.fraudRequest = "Probe" => a.disposition = "Reject"
    /\ \A a, b \in as : (a.fraudRequest = "Check" /\ b.fraudRequest = "Check") =>
            (a.disposition = b.disposition)

TypeInvariant ==
    /\ \A a \in as : a \in Attempts
                
(* The system seeks to minimize the knowledge that we transfer to any fraudster. 
If a fraudster makes a probe requst, with known false information, then the request 
will fail at the gateway and the system will return Reject confirming the 
fraudsters knowledge. Note that a probe can occur because a fraudster uses a believed 
good card with bad cvv, avs, date etc info. Or a probe can occur because a fraudster
uses a known bad card to see how the system will respond.
If a fraudster makes a check request (identified by a order, accepted by the
gateway) then we may return either Reject or Accept as long as we are consistent *)
onProbeAttempt == 
    /\ Cardinality(as) < 5 /\ customerType = "Fraud"
    /\ as' = as \cup {[fraudRequest |-> "Probe", disposition |-> "Reject"]}
    /\ UNCHANGED << customerType >>
    
onCheckAttempt ==
    /\ Cardinality(as) < 5 /\ customerType = "Fraud"
    /\ LET Prev == {a \in as : a.fraudRequest = "Check"}
       IN IF Prev /= {}
        THEN LET b == CHOOSE x \in Prev : TRUE
             IN as' = as \cup {[fraudRequest |-> "Check", disposition |-> b.disposition]}
        ELSE \E d \in DispositionType : as' = as \cup {[fraudRequest |-> "Check", disposition |-> d]}
    /\ UNCHANGED << customerType >>
    
    
BitArrayVal(b) ==
    LET n == CHOOSE i \in  Nat: DOMAIN b = 0..i
        val[i \in 0..n] == IF i = 0
                            THEN b[0] * 2^0 
                            ELSE b[i] * 2^i + val[i - 1]
    IN val[n]        

Init == 
    /\ as = {}
    /\ customerType = "Fraud" 
        
Next == 
    \/ (Cardinality(as) = 5 /\ UNCHANGED << as, customerType >>)
    \/ onProbeAttempt
    \/ onCheckAttempt

Spec == Init /\ [][Next]_<< as, customerType >>

 

=============================================================================
\* Modification History
\* Last modified Fri Jan 12 20:38:15 CST 2018 by ASUS
\* Last modified Sun Dec 24 22:17:32 CST 2017 by marco
\* Created Sun Dec 24 21:05:59 CST 2017 by marco
