------------------------- MODULE StreetsDiffUpdate -------------------------
EXTENDS Integers, SignConfigs, TLC

VARIABLES revA,    \* the set of signs in revision A 
    revB, 
    tblSigns,       \* the  
    tblCurbs,
    pc              \* process control

(*
Signs exists idependently of time. Each is associated wth a street and a
street side. Each has a well know order number which may appear in a revision
file; however the revision file may instead just show the alias order number.
*)
TypeInv ==
    /\ revA \in SUBSET Signs
    /\ revB \in SUBSET Signs
    /\ tblSigns \in SUBSET [sign: Signs,
                             order_no: Int,
                             is_active: BOOLEAN]
    /\ tblCurbs \in SUBSET [street: 1..5,
                             rl_of_street: {"R", "L"},
                             order_no: Int,
                             is_invalidated: BOOLEAN]
    /\ \A s \in (revA \cup revB): \/ s.wkOrderNo = s.aliasOrderNo
                                  \/ s.wkOrderNo = s.aliasOrderNo - 10
    /\ pc \in {"step0", "step1", "step2", "step3", "step4", "term"}

(*
On initial upload we sample from the existential universe of Signs. Some
of those signs orderNos are aliases and in future upgrades they will be
replaced by well know orderNos. We do not know which are which.
*)
DoInitialUpload == 
    /\ pc = "step0"
    /\ \E aset\in SUBSET Signs: 
                            /\ revA' = aset
                            /\ tblSigns' = {[sign |-> s, 
                                            order_no |-> s.aliasOrderNo, 
                                            is_active |-> TRUE]: s \in aset}
                            /\ tblCurbs' = {[street |-> s.street,
                                            rl_of_street |-> s.rlOfStreet,
                                            order_no |-> s.aliasOrderNo,
                                            is_invalidated |-> FALSE]: s \in aset}
    /\ pc' = "step1"
    /\ UNCHANGED << revB >>

DownloadRevisions ==
    /\ pc = "step1"
    /\ \E bset\in SUBSET Signs: revB' = bset
    /\ pc' = "step2"
    /\ UNCHANGED << revA, tblSigns, tblCurbs >>
    
(*
Once we have revisions, conceptually some well known order numbers
are made know to us through the data in revB. We can use this to
switch some of the order numbers into their well known form
*)
DoCanonicalOrderNoTransform == 
    /\ pc = "step2"
    /\ LET transform == [s \in revB |-> s.wkOrderNo]
       IN /\ tblSigns' = {[sign |-> rec.sign,
                           order_no |-> getWkOrderNoByRefSigns(rec, revB),
                           is_active |-> rec.is_active]: rec \in tblSigns}
          /\ tblCurbs' = {[street |-> rec.street,
                           rl_of_street |-> rec.rl_of_street,
                           order_no |-> calcWkOrderNoByRefSigns(rec.order_no, revB),
                           is_invalidated |-> rec.is_invalidated]: rec \in tblCurbs}
    /\ pc' = "step3"
    /\ UNCHANGED << revA, revB >>
    
(* 
Any record that has a deleted or modified order number associated with it needs to be
invalidated. For now assume that all deleted records and change (even order no) records
are invalidated
*)
RunTableInvalidation ==
    /\ pc = "step3" 
    /\ LET Invlds == UNION { (revA \ revB), {s \in revB: s.wkOrderNo % 2 = 0} }
           InvldOrderNos == {s.wkOrderNo: s \in Invlds}
        IN /\ tblSigns' = {IF rec.sign \in Invlds THEN 
                                [sign |-> rec.sign,
                                 order_no |-> rec.order_no,
                                 is_active |-> FALSE]
                           ELSE rec : rec \in tblSigns}
           /\ tblCurbs' = {IF rec.order_no \in InvldOrderNos THEN
                                [street |-> rec.street,
                                 rl_of_street |-> rec.rl_of_street,
                                 order_no |-> rec.order_no,
                                 is_invalidated |-> TRUE]
                           ELSE rec : rec \in tblCurbs}
    /\ pc' = "step4"
    /\ UNCHANGED << revA, revB >> 

(*
Any signs that are new or have sign text changes in revision B. Need to be added to the
sign table. Once these additions are done a read of the acive records in the sign table will 
be yeild a view identical to a fresh load of revision B. 
*)
AddNewSigns ==
    /\ pc = "step4"
    /\ LET Adds == revB \ revA
           Mods == {s \in revB: s.wkOrderNo % 2 = 0}    \* change records mod 2 by convention
       IN /\ tblSigns' = UNION { tblSigns, 
                                  {[sign |-> s,
                                    order_no |-> s.wkOrderNo,
                                    is_active |-> TRUE]: s \in UNION {Adds, Mods}} }
    /\ pc' = "term"
    /\ UNCHANGED << revA, revB, tblCurbs >>

CheckTermination ==
    /\ pc = "term"
    /\ LET expSigns == {[sign |-> s,
                        order_no |-> s.wkOrderNo,
                        is_active |-> TRUE]: s \in revB}
           actSign == {s \in tblSigns: s.is_active = TRUE}
       IN Assert(expSigns = actSign, "sign table matches the contents of revB")
    /\ UNCHANGED << revA, revB, tblSigns, tblCurbs, pc >>
    
Init == /\ revA = {} /\ revB = {} /\ tblSigns = {} /\ tblCurbs = {} /\ pc = "step0"
    
Next == \/ DoInitialUpload
        \/ DownloadRevisions
        \/ DoCanonicalOrderNoTransform
        \/ RunTableInvalidation
        \/ AddNewSigns
        \/ CheckTermination

Spec == Init /\ [][Next]_<< revA, revB, tblSigns, tblCurbs, pc >>
=============================================================================
\* Modification History
\* Last modified Wed Jun 17 22:34:57 CST 2015 by marco
\* Created Wed Jun 17 12:40:15 CST 2015 by marco
