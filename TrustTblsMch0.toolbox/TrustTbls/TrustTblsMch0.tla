--------------------------- MODULE TrustTblsMch0 ---------------------------
EXTENDS Integers, TLC, TrustTblsCtx0, Sequences

(* fraud detection is hueristic baseed. It looks for inconsistencies between
customer reservation attempts and when sufficient or severe enough inconsistencies
are detected marks a customer a fraud *)

(* Consider two customers which are characterized a similiar by some measure. We 
will not specify here how we determine that two customers are similiar but in
practice it would be probationary customers with the same credit card or 
remarkably similiar emails
*)
VARIABLES c1, c2, pc
(* In the model, conceptually we have c1 which is the customer of interest and
then c2 which represents and abstraction of all other similiar customers 
Customers transition from a Probationary state into either a Fraud, or possibly a Trusted, state 
and do not transition out of those states. In practice the transition characteristics imply that 
decisions must be conservative, and robust to false positives *)
InvType ==
    /\ c1 \in Customers
    /\ c2 \in Customers 
    /\ Len(c1.attempts) <= 2
    /\ Len (c2.attempts) <= 2
                
(* 
1. If a customer is marked as fraud then similiar customers must also
   be marked as fraud. Or similiar customers must have consistent trust
   Categories
2. When one customer is trusted however we assume that the similiarity 
    match is incorrect
*)
InvSimilarity ==
    /\ c1.trustCategory = "Fraud" => c2.trustCategory = "Fraud"
    /\ c2.trustCategory = "Fraud" => c1.trustCategory = "Fraud"

(* Customers that are fraudulent can make reservations but once a customer is 
identified as Fraud then his reservations must be refunded silently along with
all similiar customers *)
InvRefund == 
    /\ c1.trustCategory = "Fraud" =>
        (
            /\ \A aa \in DOMAIN c1.attempts : c1.attempts[aa] = "Refunded"
            /\ \A ab \in DOMAIN c2.attempts : c2.attempts[ab] = "Refunded"
        ) 
             
(* If a customer goes from probationary to fraud then we untrust similiar customers
and refund all the reservations associated with the customer and similiar customers. *)
onAttemptDataInconsistentProbationaryCustomer(c) == 
    /\ pc = "" 
    /\ IF c = 1
        THEN /\ (Len(c1.attempts) < 2 /\ c1.trustCategory = "Probationary")
             /\ CASE c2.trustCategory = "Trusted" ->  (* a bit contradictory, wait for more information *)
                    /\ c1' = [c1 EXCEPT !.attempts = Append(c1.attempts, "Issued")] 
                    /\ UNCHANGED << c2, pc >>
                [] c2.trustCategory = "Probationary" ->
                    /\ c1' = [c1 EXCEPT !.trustCategory = "Fraud",
                                        !.attempts = Append([i \in DOMAIN c1.attempts |-> "Refunded"], "Refunded")] 
                    /\ c2' = [c2 EXCEPT !.trustCategory = "Fraud",
                                        !.attempts = [i \in DOMAIN c2.attempts |-> "Refunded"]]
                    /\ UNCHANGED << pc >>
                [] c2.trustCategory = "Fraud" ->
                    /\ c1' = [c1 EXCEPT !.trustCategory = "Fraud",
                                        !.attempts = Append([i \in DOMAIN c1.attempts |-> "Refunded"], "Refunded")]
                    /\ UNCHANGED << c2, pc >>
        ELSE /\ (Len(c2.attempts) < 2 /\ c2.trustCategory = "Probationary")
             
             /\ CASE c1.trustCategory = "Trusted" ->
                    /\ c2' = [c2 EXCEPT !.attempts = Append(c2.attempts, "Issued")] 
                    /\ UNCHANGED << c1, pc >>
                [] c1.trustCategory = "Probationary" ->
                    /\ c2' = [c2 EXCEPT !.trustCategory = "Fraud",
                                        !.attempts = Append([i \in DOMAIN c2.attempts |-> "Refunded"], "Refunded")]
                    /\ c1' = [c1 EXCEPT !.trustCategory = "Fraud",
                                        !.attempts = [i \in DOMAIN c1.attempts |-> "Refunded"]]
                    /\ UNCHANGED << pc >>
                [] c1.trustCategory = "Fraud" ->
                    /\ c2' = [c2 EXCEPT !.trustCategory = "Fraud",
                                        !.attempts = Append([i \in DOMAIN c2.attempts |-> "Refunded"], "Refunded")]
                    /\ UNCHANGED << c1, pc >>

(* If there are no conflicts then just leave everything as is *)
onAttemptDataConsistentProbationaryCustomer(c) ==
    /\ pc = "" 
    /\ IF c = 1
        THEN /\ (Len(c1.attempts) < 2 /\ c1.trustCategory = "Probationary") 
             /\ c1' = [c1 EXCEPT !.attempts = Append(c1.attempts, "Issued")]
             /\ UNCHANGED << pc, c2 >>
        ELSE /\ (Len(c2.attempts) < 2 /\ c2.trustCategory = "Probationary")
             /\ c2' = [c2 EXCEPT !.attempts = Append(c2.attempts, "Issued")]
             /\ UNCHANGED << pc, c1 >>
             
(* If a customer is trusted then we continue to trust him, and leave reservations
as issued *)
onAttemptTrustedCustomer(c) == 
    /\ pc = ""
    /\ IF c = 1
        THEN /\ (Len(c1.attempts) < 2 /\ c1.trustCategory = "Trusted")
             /\ c1' = [c1 EXCEPT !.attempts = Append(c1.attempts, "Issued")]
             /\ UNCHANGED << c2, pc >>
        ELSE /\ (Len(c2.attempts) < 2 /\ c2.trustCategory = "Trusted")
             /\ c2' = [c2 EXCEPT !.attempts = Append(c2.attempts, "Issued")]
             /\ UNCHANGED << c1, pc >>
             
(* If a customer is fruad then ancel all additional reservations *)
onAttemptFraudCustomer(c) == 
    /\ pc = ""
    /\ IF c = 1
        THEN /\ (Len(c1.attempts) < 2 /\ c1.trustCategory = "Fraud")
             /\ c1' = [c1 EXCEPT !.attempts = Append(c1.attempts, "Refunded")]
             /\ UNCHANGED << c2, pc >>
        ELSE /\ (Len(c2.attempts) < 2 /\ c2.trustCategory = "Fraud")
             /\ c2' = [c2 EXCEPT !.attempts = Append(c2.attempts, "Refunded")]
             /\ UNCHANGED << c1, pc >>             

(* Happens when the customer has been confirmed as trusted. The trusting happens
some time after the first reservation if there has not been any trouble with the
customer *)
onTrust(c) ==
    /\ pc = "" 
    /\ IF c = 1
        THEN /\ (Len(c1.attempts) /= 0 /\ c1.trustCategory = "Probationary")
             /\ c1' = [c1 EXCEPT !.trustCategory = "Trusted"]
             /\ UNCHANGED << c2, pc >>
        ELSE /\ (Len(c2.attempts) /= 0 /\ c2.trustCategory = "Probationary")
             /\ c2' = [c2 EXCEPT !.trustCategory = "Trusted"]
             /\ UNCHANGED << c1, pc >>

onDone ==
    /\ (pc = "" /\ Len(c1.attempts) = 2 /\ Len(c2.attempts) = 2)
    /\ pc' = "done"
    /\ UNCHANGED << c1, c2 >>
    
(* In the initial state we have one probationary customer with an issued 
reservation. From here we start adding reservation attempts and watch for 
data inconsistencies that alert us to fraud *)
Init == 
    /\ c1 = [
            trustCategory |-> "Probationary",
            attempts |-> << "Issued" >>
        ] 
    /\ c2 = [
            trustCategory |-> "Probationary",
            attempts |-> << >>
        ]
    /\ pc = ""
        
Next == 
    \/ (pc = "done" /\ UNCHANGED << c1, c2, pc >>)
    \/ onDone
    \/ \E c \in {1, 2} :   
        \/ onTrust(c) 
        \/ onAttemptDataConsistentProbationaryCustomer(c) 
        \/ onAttemptFraudCustomer(c) 
        \/ onAttemptDataInconsistentProbationaryCustomer(c)
        \/ onAttemptTrustedCustomer(c)

Spec == Init /\ [][Next]_<< c1, c2, pc >>
=============================================================================
\* Modification History
\* Last modified Sun Sep 10 20:01:16 CST 2017 by ASUS
\* Created Tue Aug 29 11:57:56 CST 2017 by ASUS