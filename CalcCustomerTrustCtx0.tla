----------------------- MODULE CalcCustomerTrustCtx0 -----------------------
EXTENDS Integers

(* All customers are separated into three categories. Customers with a purchase
history will generally be either Trusted or Fraud. New customers w
*)
TrustCategory == {"Probationary", "Trusted", "Fraud"}
Email == {"Matches", "NotMatches", "Similiar"}

ReservationStatus == {"Issued", "Refunded"}



Attempts == [
        creditCard: Int, 
        deviceId: Int,
        reservationStatus: ReservationStatus
    ]
    
Customers == [
        trustCategory: TrustCategory,
        email: Email,
        attempts: Attempts
    ]


=============================================================================
\* Modification History
\* Last modified Mon Oct 23 08:14:40 CST 2017 by ASUS
\* Created Tue Oct 03 13:24:56 CST 2017 by ASUS
