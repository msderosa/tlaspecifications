-------------------------------- MODULE Area --------------------------------
EXTENDS Integers

_intersects(area, ref) ==
    \/ (ref.sw >= area.sw /\ ref.sw < area.ne)
    \/ (ref.ne > area.sw /\ ref.ne <= area.ne)
    \/ (ref.sw <= area.sw /\ ref.ne >= area.ne)
    
_split(area) ==
    IF area.sw < area.ne
        THEN {area}
        ELSE { [sw |-> area.sw, ne |-> 18],
                [sw |-> -18, ne |-> area.ne] }
                
intersects(area, ref) ==
    LET as == _split(area)
        rs == _split(ref)
    IN \E a \in as, r \in rs: _intersects(a, r)

width(area) ==
    IF area.sw < area.ne
        THEN area.ne - area.sw
        ELSE (18 - area.sw) + (area.ne + 18)
        
increment(area) == 
    IF width(area) >= 35
        THEN area
        ELSE LET high == ((area.ne + 18 + 1) % 36) - 18
                  low == IF (area.sw + 18) = 0
                          THEN 17
                          ELSE ((area.sw + 18 - 1) % 36) - 18
              IN [sw |-> low, ne |-> high]
              
decrement(area) ==
    IF width(area) <= 2
        THEN area
        ELSE LET high == IF (area.ne + 18) = 0
                            THEN 17
                            ELSE ((area.ne + 18 - 1) % 36) - 18
                  low == ((area.sw + 18 + 1) % 36) - 18
              IN [sw |-> low, ne |-> high]
              
=============================================================================
\* Modification History
\* Last modified Sun Nov 16 19:59:39 CST 2014 by marco
\* Created Sun Nov 16 15:56:00 CST 2014 by marco
