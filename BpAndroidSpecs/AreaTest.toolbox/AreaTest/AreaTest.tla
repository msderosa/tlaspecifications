------------------------------ MODULE AreaTest ------------------------------
EXTENDS Area, TLC

VARIABLES bnds

Inv == width(bnds) <= 36

TestWidth == /\ width([sw |-> -3, ne |-> 3]) = 6
             /\ width([sw |-> 16, ne |-> -17]) = 3

TestIncDec == 
    /\ (width(bnds) > 1 /\ width(bnds) < 35) =>
        (bnds = decrement(increment(bnds)))
    /\ LET t == decrement([sw |-> 8, ne |-> -18])
       IN /\ PrintT(t)
          /\ t = [sw |-> 9, ne |-> 17]
    
TestIntersects == 
    /\ intersects(
        [sw |-> 2, ne |-> 10],
        [sw |-> 8, ne |-> 15])
    /\ intersects(
        [sw |-> 12, ne |-> -15],
        [sw |-> 8, ne |-> 15])

Tests == /\ TestWidth
         /\ TestIncDec
         /\ TestIntersects
         
Init == bnds = [sw |-> -3, ne |-> 3]

Next == \/ bnds' = increment(bnds)
        \/ bnds' = decrement(bnds)
        
Spec == Init /\ [][Next]_<< bnds >>

=============================================================================
\* Modification History
\* Last modified Sun Nov 16 19:58:20 CST 2014 by marco
\* Created Sun Nov 16 19:18:18 CST 2014 by marco
