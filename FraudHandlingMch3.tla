------------------------- MODULE FraudHandlingMch3 -------------------------
EXTENDS FraudHandlingCtx3, Integers, FiniteSets

VARIABLES as, customerType, fraudProbability

M1 == INSTANCE FraudHandlingMch1

(* propose: we should model the system from the point of view of the attacker. The
the property that determines correctness of the algorithm should be that what the
attacker observes from the outside of the system can not be used to make conclusive
determinations of the card state.
assume: the attacker can trivially change the id of his device, ip address, and email.
further assume that he can make multiple sequential requests with an arbitrary small
time lag between requests, futher assume that a 
card fingerprint is available. Further assume that the attacker has full knowlege of 
the physical card data and its associated personal data.
*)
(* We are building a closed model containing the attacker and the selling application.
We want our model to be correct given the assumptions about the attacker above 
*)
(*-------------- functions ---------------------------------*)
Rand == \E n \in 0..10 : n

ProcessInvariant == M1!ProcessInvariant

TypeInvariant == 
    /\ M1!TypeInvariant
    /\ fraudProbability \in Probabilities
    
    
(* We could suspect a customer for any reason. Maybe they are using a
lot of credit cards, maybe we their email looks wierd, or there is a
fraud associated with the reserving device *)
onSuspectMore == 
    \E p \in Probabilities : /\ p > fraudProbability
                             /\ fraudProbability' = p
                             /\ IF p = 10 
                                THEN /\ customerType = "Fraud"
                                     /\ UNCHANGED << as >>
                                ELSE UNCHANGED << as , customerType >>
                                
onProbationaryAttempt ==
    /\ Cardinality(as) < 5 /\ customerType = "Probationary"
    /\ \E d \in DispositionType :
        IF d = "Accept"
        THEN as' = as \cup {[fraudRequest |-> "Check", disposition |-> d]}
        ELSE as' = as \cup {[fraudRequest |-> "Probe", disposition |-> d]}
    /\ UNCHANGED << customerType >>                                

Init == 
    /\ as = {}
    /\ \E ct \in CustomerType : customerType = ct
    /\ fraudProbability = 0 
        
Next == 
    \/ (Cardinality(as) = 5 /\ UNCHANGED << as, customerType, fraudProbability >>)
    \/ onSuspectMore

Spec == Init /\ [][Next]_<< as, customerType, fraudProbability >> 


=============================================================================
\* Modification History
\* Last modified Tue Feb 06 13:45:18 CST 2018 by ASUS
\* Created Thu Jan 04 21:15:33 CST 2018 by ASUS
