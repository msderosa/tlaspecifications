------------------------- MODULE FraudHandlingMch1 -------------------------
EXTENDS Integers, TLC, FraudHandlingCtx0, FiniteSets
VARIABLES as, customerType

M0 == INSTANCE FraudHandlingMch0

ProcessInvariant == M0!ProcessInvariant

(* if a card fails for any reason cvv or avs or data -- what happens
 if we have all further attempts with that card fail? do we leak info *)
 
(* here we extend the precondition so that customers can start off as 
probationary and then later transition to fraud
*)
TypeInvariant == 
    /\ M0!TypeInvariant
    /\ customerType \in CustomerType

(* A probationary customer could be Trusted or Fraud. Since we dont know 
assume reservation failures are probes and reservation success is a check
*)
onProbationaryAttempt ==
    /\ Cardinality(as) < 5 /\ customerType = "Probationary"
    /\ \E d \in DispositionType :
        IF d = "Accept"
        THEN as' = as \cup {[fraudRequest |-> "Check", disposition |-> d]}
        ELSE as' = as \cup {[fraudRequest |-> "Probe", disposition |-> d]}
    /\ UNCHANGED << customerType >>
    
onFraudAttempt ==
    \/ M0!onProbeAttempt
    \/ M0!onCheckAttempt
    
onDiscoveredFraud ==
    /\ Cardinality(as) < 5 /\ customerType = "Probationary"
    /\ customerType' = "Fraud"
    /\ UNCHANGED << as >>

Init == 
    /\ as = {}
    /\ \E ct \in CustomerType : customerType = ct 
        
Next == 
    \/ (Cardinality(as) = 5 /\ UNCHANGED << as, customerType >>)
    \/ onFraudAttempt
    \/ onProbationaryAttempt
    \/ onDiscoveredFraud

Spec == Init /\ [][Next]_<< as, customerType >> 

=============================================================================
\* Modification History
\* Last modified Thu Feb 01 09:04:33 CST 2018 by ASUS
\* Created Thu Jan 04 20:33:27 CST 2018 by ASUS
