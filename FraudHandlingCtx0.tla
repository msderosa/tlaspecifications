------------------------- MODULE FraudHandlingCtx0 -------------------------
CONSTANTS Attempts (* the set of attempts associated with credit card *)

FraudRequestType == {"Probe", "Check"}
(*  A Probe request is a request from a fraudster for which one or more of
the input fields (1) exp. date, (2) cvv, or (3) avs is delibrately incorrect. A Check
request is a request from a fraudster for which all information is 
correct to the extent known by the fraudster.
*)

DispositionType == {"Accept", "Reject"}
(* the final descision on the purchase represented by the card. *)

CustomerType == {"Fraud", "Probationary"}

=============================================================================
\* Modification History
\* Last modified Thu Jan 04 19:43:19 CST 2018 by ASUS
\* Last modified Sun Dec 24 21:59:51 CST 2017 by marco
\* Created Sun Dec 24 21:05:09 CST 2017 by marco
