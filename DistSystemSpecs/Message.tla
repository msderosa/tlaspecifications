------------------------------ MODULE Message ------------------------------
EXTENDS Integers

LookupReq == [type: {"Lookup"},
              node: Int]       \* the key to lookup
              
NoLegalRouteRsp == [type: {"NoLegalRoute"},
                    key: Int]      \* the key which was not found
(* 
*)                    
JoinReq == [type: {"JoinRequest"},
            node: Int]          \* the node which wants to join the network
            
JoinRsp == [type: {"JoinReply"},
            rtable: SUBSET Int,
            lset: SUBSET Int]
LSProbeReq == [type: {"LSProbe"},
               node: Int,          \* the node sending the request
               lset: SUBSET Int,
               failed: SUBSET Int]
               
LSProbeRsp == [type: {"LSProbeReply"},
               node: Int,           \* the node sending the reply
               lset: SUBSET Int,
               failed: SUBSET Int]
               
LeaseReq == [type: {"LeaseRequest"},
             node: Int]             \* the node sending the request
LeaseRsp == [type: {"BroadcastLSet"},
             lset: Int]

DataPacket == UNION {LookupReq, JoinReq, LSProbeReq, LeaseReq,
                      NoLegalRouteRsp, JoinRsp, LSProbeRsp, LeaseRsp}


Message == [destination: Int,   \* the node to which the message is adressed
            mreq: DataPacket]
            
=============================================================================
\* Modification History
\* Last modified Tue Jan 19 20:48:32 CST 2016 by marco
\* Created Sat Jul 18 15:40:31 CST 2015 by marco
