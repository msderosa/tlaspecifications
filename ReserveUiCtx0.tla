--------------------------- MODULE ReserveUiCtx0 ---------------------------
(* This is the high level model for reservation checkout pages. The process
    is abstracted so the context states and machine events should be used
    as a guide to behavior and understanding, and not as a guide to
    implementation
*)

(* All customers start out as Trusted so trusted may mean that we have previous
    information that gives us reason to trust this customer or it may mean that
    we have no previous information and so on reason to not trust him
 *)
TrustDesignation == {"Trusted", "Fraudster", "Banned"}

ProcessControl == {
    "PreviewStart", "DataStepOk", "NonceStepOk", 
    "NonceStepFraud", "DoneErr", "DoneOk", "DoneFraud"
    }

(* The customer trust designation will be returned in the response from the preview
    API
*)
Customers == [
    trustDesignation: TrustDesignation
]

NonceRsp == {
    [success |-> TRUE, error |-> {}],
    [success |-> FALSE, error |-> "AvsOrCvv"],
    [success |-> FALSE, error |-> "AllOther"]
}

=============================================================================
\* Modification History
\* Last modified Mon Aug 21 18:43:11 CST 2017 by marco
\* Created Mon Aug 21 15:53:46 CST 2017 by marco
