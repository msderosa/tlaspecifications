------------------------ MODULE TerminationDetection ------------------------
(*
This specification details how in flight requests are monitored in order to 
activate or terminate the map page loading animation.
*)
EXTENDS Integers, FiniteSets

(*
Each data request process is tracked with a Token and its associated sequence
number. The tasks themselves are stored in a running task list. The state of the map
loading indicator is tracked by the indicator variable below.
*)
VARIABLES tokenSeq, runningTasks, indicator

Init == /\ tokenSeq = -1
        /\ runningTasks = [tokenSeq |-> -1, tasks |-> {}]
        /\ indicator = "Passive"

TypeInv == /\ tokenSeq \in -1..10
           /\ runningTasks \in [tokenSeq: -1..10, tasks: SUBSET (0..10)]
           /\ indicator \in {"Active", "Passive"}

RunningTasksInv == \A n \in runningTasks.tasks: n >= runningTasks.tokenSeq
IndicatorInv == IF Cardinality(runningTasks.tasks) > 0
                    THEN indicator = "Active"
                    ELSE indicator = "Passive"
                    
Inv == /\ TypeInv
       /\ RunningTasksInv
       /\ IndicatorInv

\* release a task to retrieve data
ReleaseProcess == 
    /\ tokenSeq < 10
    /\ tokenSeq' = tokenSeq + 1
    /\ runningTasks' = [runningTasks EXCEPT !.tasks = UNION {runningTasks.tasks, {tokenSeq + 1}}]
    /\ indicator' = "Active"

\* a task has finished returning data
ReceiveProcess == 
    /\ Cardinality(runningTasks.tasks) > 0
    /\ \E n \in runningTasks.tasks: 
            LET ValidTasks == {i \in runningTasks.tasks: i >= n}
            IN /\ runningTasks' = [runningTasks EXCEPT !.tokenSeq = n, !.tasks = ValidTasks]
               /\ indicator' = IF ValidTasks = {} 
                               THEN "Passive"
                               ELSE "Active"
    /\ UNCHANGED << tokenSeq >>
                                                                
Next == \/ ReleaseProcess
        \/ ReceiveProcess
        
Spec == Init /\ [][Next]_<< tokenSeq, runningTasks, indicator >>

=============================================================================
\* Modification History
\* Last modified Thu Oct 09 19:08:46 CST 2014 by marco
\* Created Wed Oct 01 21:25:08 CST 2014 by marco
